/********************************************************************************
** Form generated from reading UI file 'returnbookform.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RETURNBOOKFORM_H
#define UI_RETURNBOOKFORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ReturnBookForm
{
public:
    QCheckBox *huHongCheckBox;
    QSpinBox *huHongSpin;
    QCheckBox *quaHanCheckBox;
    QSpinBox *quaHanSpin;
    QCheckBox *matSachCheckBox;
    QSpinBox *matSachSpin;
    QLabel *label;
    QSpinBox *quaHanSpin2;
    QSpinBox *huHongSpin2;
    QSpinBox *matSachSpin_2;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QPushButton *pushButton;
    QPushButton *exitButton;
    QFrame *line;
    QLineEdit *phat;
    QLabel *label_16;
    QLineEdit *chuThich;
    QLabel *label_18;
    QWidget *layoutWidget;
    QGridLayout *gridLayout;
    QLabel *label_7;
    QLabel *label_15;
    QLabel *label_8;
    QLabel *label_10;
    QLabel *label_12;
    QLabel *label_14;
    QLabel *label_17;
    QWidget *layoutWidget1;
    QGridLayout *gridLayout_2;
    QLabel *maSach;
    QLabel *giaSach;
    QLabel *userName;
    QLabel *hoTen;
    QLabel *MSSV;
    QLabel *CMND;
    QLabel *SL;

    void setupUi(QWidget *ReturnBookForm)
    {
        if (ReturnBookForm->objectName().isEmpty())
            ReturnBookForm->setObjectName(QStringLiteral("ReturnBookForm"));
        ReturnBookForm->resize(330, 496);
        ReturnBookForm->setMaximumSize(QSize(330, 496));
        huHongCheckBox = new QCheckBox(ReturnBookForm);
        huHongCheckBox->setObjectName(QStringLiteral("huHongCheckBox"));
        huHongCheckBox->setGeometry(QRect(30, 270, 97, 20));
        QFont font;
        font.setPointSize(11);
        huHongCheckBox->setFont(font);
        huHongCheckBox->setTristate(false);
        huHongSpin = new QSpinBox(ReturnBookForm);
        huHongSpin->setObjectName(QStringLiteral("huHongSpin"));
        huHongSpin->setGeometry(QRect(134, 272, 40, 22));
        huHongSpin->setFont(font);
        quaHanCheckBox = new QCheckBox(ReturnBookForm);
        quaHanCheckBox->setObjectName(QStringLiteral("quaHanCheckBox"));
        quaHanCheckBox->setGeometry(QRect(30, 242, 99, 20));
        quaHanCheckBox->setFont(font);
        quaHanCheckBox->setTristate(false);
        quaHanSpin = new QSpinBox(ReturnBookForm);
        quaHanSpin->setObjectName(QStringLiteral("quaHanSpin"));
        quaHanSpin->setGeometry(QRect(134, 244, 40, 22));
        quaHanSpin->setFont(font);
        matSachCheckBox = new QCheckBox(ReturnBookForm);
        matSachCheckBox->setObjectName(QStringLiteral("matSachCheckBox"));
        matSachCheckBox->setGeometry(QRect(30, 298, 98, 20));
        matSachCheckBox->setFont(font);
        matSachCheckBox->setTristate(false);
        matSachSpin = new QSpinBox(ReturnBookForm);
        matSachSpin->setObjectName(QStringLiteral("matSachSpin"));
        matSachSpin->setGeometry(QRect(134, 300, 40, 22));
        matSachSpin->setFont(font);
        label = new QLabel(ReturnBookForm);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(70, 10, 171, 31));
        QFont font1;
        font1.setPointSize(16);
        font1.setBold(true);
        font1.setWeight(75);
        label->setFont(font1);
        quaHanSpin2 = new QSpinBox(ReturnBookForm);
        quaHanSpin2->setObjectName(QStringLiteral("quaHanSpin2"));
        quaHanSpin2->setGeometry(QRect(195, 244, 101, 22));
        quaHanSpin2->setFont(font);
        quaHanSpin2->setMaximum(10000000);
        quaHanSpin2->setSingleStep(1000);
        huHongSpin2 = new QSpinBox(ReturnBookForm);
        huHongSpin2->setObjectName(QStringLiteral("huHongSpin2"));
        huHongSpin2->setGeometry(QRect(195, 272, 101, 22));
        huHongSpin2->setFont(font);
        huHongSpin2->setMaximum(10000000);
        huHongSpin2->setSingleStep(1000);
        matSachSpin_2 = new QSpinBox(ReturnBookForm);
        matSachSpin_2->setObjectName(QStringLiteral("matSachSpin_2"));
        matSachSpin_2->setGeometry(QRect(195, 300, 101, 22));
        matSachSpin_2->setFont(font);
        matSachSpin_2->setMaximum(10000000);
        matSachSpin_2->setSingleStep(1000);
        label_2 = new QLabel(ReturnBookForm);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(180, 244, 16, 19));
        QFont font2;
        font2.setPointSize(11);
        font2.setBold(false);
        font2.setWeight(50);
        label_2->setFont(font2);
        label_3 = new QLabel(ReturnBookForm);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(180, 272, 16, 19));
        label_3->setFont(font2);
        label_4 = new QLabel(ReturnBookForm);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(180, 300, 16, 19));
        label_4->setFont(font2);
        pushButton = new QPushButton(ReturnBookForm);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(70, 460, 81, 23));
        QFont font3;
        font3.setPointSize(12);
        font3.setBold(true);
        font3.setWeight(75);
        pushButton->setFont(font3);
        exitButton = new QPushButton(ReturnBookForm);
        exitButton->setObjectName(QStringLiteral("exitButton"));
        exitButton->setGeometry(QRect(190, 460, 75, 23));
        exitButton->setFont(font3);
        line = new QFrame(ReturnBookForm);
        line->setObjectName(QStringLiteral("line"));
        line->setGeometry(QRect(10, 390, 301, 16));
        line->setFont(font);
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);
        phat = new QLineEdit(ReturnBookForm);
        phat->setObjectName(QStringLiteral("phat"));
        phat->setGeometry(QRect(100, 420, 141, 21));
        phat->setFont(font);
        label_16 = new QLabel(ReturnBookForm);
        label_16->setObjectName(QStringLiteral("label_16"));
        label_16->setGeometry(QRect(30, 420, 71, 20));
        label_16->setFont(font);
        chuThich = new QLineEdit(ReturnBookForm);
        chuThich->setObjectName(QStringLiteral("chuThich"));
        chuThich->setGeometry(QRect(100, 350, 191, 21));
        chuThich->setFont(font);
        label_18 = new QLabel(ReturnBookForm);
        label_18->setObjectName(QStringLiteral("label_18"));
        label_18->setGeometry(QRect(30, 350, 61, 20));
        label_18->setFont(font);
        layoutWidget = new QWidget(ReturnBookForm);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(30, 51, 78, 164));
        layoutWidget->setFont(font);
        gridLayout = new QGridLayout(layoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        label_7 = new QLabel(layoutWidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        QFont font4;
        font4.setPointSize(11);
        font4.setBold(true);
        font4.setWeight(75);
        label_7->setFont(font4);

        gridLayout->addWidget(label_7, 0, 0, 1, 1);

        label_15 = new QLabel(layoutWidget);
        label_15->setObjectName(QStringLiteral("label_15"));
        label_15->setFont(font4);

        gridLayout->addWidget(label_15, 1, 0, 1, 1);

        label_8 = new QLabel(layoutWidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setFont(font4);

        gridLayout->addWidget(label_8, 2, 0, 1, 1);

        label_10 = new QLabel(layoutWidget);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setFont(font4);

        gridLayout->addWidget(label_10, 3, 0, 1, 1);

        label_12 = new QLabel(layoutWidget);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setFont(font4);

        gridLayout->addWidget(label_12, 4, 0, 1, 1);

        label_14 = new QLabel(layoutWidget);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setFont(font4);

        gridLayout->addWidget(label_14, 5, 0, 1, 1);

        label_17 = new QLabel(layoutWidget);
        label_17->setObjectName(QStringLiteral("label_17"));
        label_17->setFont(font4);

        gridLayout->addWidget(label_17, 6, 0, 1, 1);

        layoutWidget1 = new QWidget(ReturnBookForm);
        layoutWidget1->setObjectName(QStringLiteral("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(120, 51, 181, 164));
        layoutWidget1->setFont(font);
        gridLayout_2 = new QGridLayout(layoutWidget1);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        maSach = new QLabel(layoutWidget1);
        maSach->setObjectName(QStringLiteral("maSach"));
        maSach->setFont(font);

        gridLayout_2->addWidget(maSach, 0, 0, 1, 1);

        giaSach = new QLabel(layoutWidget1);
        giaSach->setObjectName(QStringLiteral("giaSach"));
        giaSach->setFont(font);

        gridLayout_2->addWidget(giaSach, 1, 0, 1, 1);

        userName = new QLabel(layoutWidget1);
        userName->setObjectName(QStringLiteral("userName"));
        userName->setFont(font);

        gridLayout_2->addWidget(userName, 2, 0, 1, 1);

        hoTen = new QLabel(layoutWidget1);
        hoTen->setObjectName(QStringLiteral("hoTen"));
        hoTen->setFont(font);

        gridLayout_2->addWidget(hoTen, 3, 0, 1, 1);

        MSSV = new QLabel(layoutWidget1);
        MSSV->setObjectName(QStringLiteral("MSSV"));
        MSSV->setFont(font);

        gridLayout_2->addWidget(MSSV, 4, 0, 1, 1);

        CMND = new QLabel(layoutWidget1);
        CMND->setObjectName(QStringLiteral("CMND"));
        CMND->setFont(font);

        gridLayout_2->addWidget(CMND, 5, 0, 1, 1);

        SL = new QLabel(layoutWidget1);
        SL->setObjectName(QStringLiteral("SL"));
        SL->setFont(font);

        gridLayout_2->addWidget(SL, 6, 0, 1, 1);


        retranslateUi(ReturnBookForm);

        QMetaObject::connectSlotsByName(ReturnBookForm);
    } // setupUi

    void retranslateUi(QWidget *ReturnBookForm)
    {
        ReturnBookForm->setWindowTitle(QApplication::translate("ReturnBookForm", "Tr\341\272\243/ph\341\272\241t s\303\241ch", Q_NULLPTR));
        huHongCheckBox->setText(QApplication::translate("ReturnBookForm", "H\306\260 h\341\273\217ng", Q_NULLPTR));
        quaHanCheckBox->setText(QApplication::translate("ReturnBookForm", "Qu\303\241 h\341\272\241n       ", Q_NULLPTR));
        matSachCheckBox->setText(QApplication::translate("ReturnBookForm", "M\341\272\245t s\303\241ch      ", Q_NULLPTR));
        label->setText(QApplication::translate("ReturnBookForm", "Ph\341\272\241t/Tr\341\272\243 s\303\241ch", Q_NULLPTR));
        label_2->setText(QApplication::translate("ReturnBookForm", "X", Q_NULLPTR));
        label_3->setText(QApplication::translate("ReturnBookForm", "X", Q_NULLPTR));
        label_4->setText(QApplication::translate("ReturnBookForm", "X", Q_NULLPTR));
        pushButton->setText(QApplication::translate("ReturnBookForm", "Ph\341\272\241t/Tr\341\272\243", Q_NULLPTR));
        exitButton->setText(QApplication::translate("ReturnBookForm", "Tho\303\241t", Q_NULLPTR));
        label_16->setText(QApplication::translate("ReturnBookForm", "Ph\303\255 ph\341\272\241t", Q_NULLPTR));
        chuThich->setText(QString());
        label_18->setText(QApplication::translate("ReturnBookForm", "Ch\303\272 th\303\255ch", Q_NULLPTR));
        label_7->setText(QApplication::translate("ReturnBookForm", "M\303\243 s\303\241ch", Q_NULLPTR));
        label_15->setText(QApplication::translate("ReturnBookForm", "Gi\303\241 s\303\241ch", Q_NULLPTR));
        label_8->setText(QApplication::translate("ReturnBookForm", "userName", Q_NULLPTR));
        label_10->setText(QApplication::translate("ReturnBookForm", "H\341\273\215 t\303\252n", Q_NULLPTR));
        label_12->setText(QApplication::translate("ReturnBookForm", "MSSV/GV", Q_NULLPTR));
        label_14->setText(QApplication::translate("ReturnBookForm", "CMND", Q_NULLPTR));
        label_17->setText(QApplication::translate("ReturnBookForm", "S\341\273\221 l\306\260\341\273\243ng", Q_NULLPTR));
        maSach->setText(QApplication::translate("ReturnBookForm", "maSach", Q_NULLPTR));
        giaSach->setText(QApplication::translate("ReturnBookForm", "giaSach", Q_NULLPTR));
        userName->setText(QApplication::translate("ReturnBookForm", "userName", Q_NULLPTR));
        hoTen->setText(QApplication::translate("ReturnBookForm", "hoTen", Q_NULLPTR));
        MSSV->setText(QApplication::translate("ReturnBookForm", "MSSV", Q_NULLPTR));
        CMND->setText(QApplication::translate("ReturnBookForm", "CMND", Q_NULLPTR));
        SL->setText(QApplication::translate("ReturnBookForm", "SL", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class ReturnBookForm: public Ui_ReturnBookForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RETURNBOOKFORM_H
