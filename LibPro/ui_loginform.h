/********************************************************************************
** Form generated from reading UI file 'loginform.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGINFORM_H
#define UI_LOGINFORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LoginForm
{
public:
    QHBoxLayout *horizontalLayout_12;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_3;
    QLabel *label_8;
    QLabel *label_5;
    QSpacerItem *horizontalSpacer_4;
    QHBoxLayout *horizontalLayout_5;
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_2;
    QLabel *label_11;
    QFormLayout *formLayout;
    QLabel *label_3;
    QLabel *label_4;
    QLineEdit *userNameEdit;
    QLineEdit *passWordEdit;
    QPushButton *loginButton;
    QSpacerItem *verticalSpacer_5;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *signUpButton;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_5;
    QLabel *label_12;
    QVBoxLayout *verticalLayout_4;
    QPushButton *muaTaiLieuButton;
    QPushButton *gopYButton;
    QPushButton *noiQuyButton;
    QHBoxLayout *horizontalLayout_3;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_2;
    QVBoxLayout *verticalLayout;
    QWidget *book;
    QGridLayout *gridLayout;
    QPushButton *viewAllButton;
    QSpacerItem *verticalSpacer;
    QPushButton *searchButton;
    QLineEdit *searchBox;
    QSpacerItem *verticalSpacer_3;
    QLabel *label_7;
    QLabel *label;
    QSpacerItem *verticalSpacer_7;
    QSpacerItem *verticalSpacer_4;
    QLabel *label_9;
    QComboBox *searchComboBox;
    QLabel *label_10;
    QComboBox *sortModeComboBox;
    QComboBox *typeComboBox;
    QComboBox *sortComboBox;
    QSpacerItem *verticalSpacer_2;
    QTableView *bookTableView;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *viewMoreButton;
    QSpacerItem *horizontalSpacer;
    QLabel *label_6;

    void setupUi(QWidget *LoginForm)
    {
        if (LoginForm->objectName().isEmpty())
            LoginForm->setObjectName(QStringLiteral("LoginForm"));
        LoginForm->resize(1360, 710);
        LoginForm->setMinimumSize(QSize(1360, 630));
        LoginForm->setMaximumSize(QSize(16777215, 16777215));
        QIcon icon;
        icon.addFile(QStringLiteral(":/images/LibPro.ico"), QSize(), QIcon::Normal, QIcon::Off);
        LoginForm->setWindowIcon(icon);
        LoginForm->setAutoFillBackground(true);
        LoginForm->setStyleSheet(QStringLiteral(""));
        horizontalLayout_12 = new QHBoxLayout(LoginForm);
        horizontalLayout_12->setObjectName(QStringLiteral("horizontalLayout_12"));
        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_3);

        label_8 = new QLabel(LoginForm);
        label_8->setObjectName(QStringLiteral("label_8"));
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_8->sizePolicy().hasHeightForWidth());
        label_8->setSizePolicy(sizePolicy);
        label_8->setMaximumSize(QSize(80, 80));
        label_8->setPixmap(QPixmap(QString::fromUtf8(":/images/LibPro.ico")));
        label_8->setScaledContents(true);

        horizontalLayout_4->addWidget(label_8);

        label_5 = new QLabel(LoginForm);
        label_5->setObjectName(QStringLiteral("label_5"));
        QFont font;
        font.setFamily(QStringLiteral("Segoe UI Black"));
        font.setPointSize(28);
        font.setBold(true);
        font.setWeight(75);
        label_5->setFont(font);

        horizontalLayout_4->addWidget(label_5);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_4);


        verticalLayout_6->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        groupBox = new QGroupBox(LoginForm);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy1);
        groupBox->setMaximumSize(QSize(300, 16777215));
        QFont font1;
        font1.setPointSize(11);
        font1.setBold(true);
        font1.setWeight(75);
        groupBox->setFont(font1);
        groupBox->setContextMenuPolicy(Qt::NoContextMenu);
        groupBox->setStyleSheet(QLatin1String("#groupBox {\n"
"border: 1px solid black;\n"
"}"));
        groupBox->setFlat(false);
        groupBox->setCheckable(false);
        groupBox->setChecked(false);
        verticalLayout_3 = new QVBoxLayout(groupBox);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout_3->addWidget(label_2);

        label_11 = new QLabel(groupBox);
        label_11->setObjectName(QStringLiteral("label_11"));

        verticalLayout_3->addWidget(label_11);

        formLayout = new QFormLayout();
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));
        QFont font2;
        font2.setPointSize(10);
        font2.setBold(true);
        font2.setWeight(75);
        label_3->setFont(font2);

        formLayout->setWidget(0, QFormLayout::LabelRole, label_3);

        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setFont(font2);

        formLayout->setWidget(2, QFormLayout::LabelRole, label_4);

        userNameEdit = new QLineEdit(groupBox);
        userNameEdit->setObjectName(QStringLiteral("userNameEdit"));
        QFont font3;
        font3.setPointSize(12);
        userNameEdit->setFont(font3);
        userNameEdit->setMaxLength(20);
        userNameEdit->setCursorPosition(0);

        formLayout->setWidget(1, QFormLayout::SpanningRole, userNameEdit);

        passWordEdit = new QLineEdit(groupBox);
        passWordEdit->setObjectName(QStringLiteral("passWordEdit"));
        passWordEdit->setFont(font3);
        passWordEdit->setEchoMode(QLineEdit::Password);

        formLayout->setWidget(3, QFormLayout::SpanningRole, passWordEdit);


        verticalLayout_3->addLayout(formLayout);

        loginButton = new QPushButton(groupBox);
        loginButton->setObjectName(QStringLiteral("loginButton"));
        QFont font4;
        font4.setPointSize(12);
        font4.setBold(true);
        font4.setWeight(75);
        loginButton->setFont(font4);
        loginButton->setAutoDefault(false);

        verticalLayout_3->addWidget(loginButton);

        verticalSpacer_5 = new QSpacerItem(20, 55, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_5);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        signUpButton = new QPushButton(groupBox);
        signUpButton->setObjectName(QStringLiteral("signUpButton"));
        signUpButton->setFont(font1);

        horizontalLayout_2->addWidget(signUpButton);


        verticalLayout_3->addLayout(horizontalLayout_2);


        verticalLayout_2->addWidget(groupBox);

        groupBox_2 = new QGroupBox(LoginForm);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(groupBox_2->sizePolicy().hasHeightForWidth());
        groupBox_2->setSizePolicy(sizePolicy2);
        groupBox_2->setMaximumSize(QSize(300, 170));
        groupBox_2->setFont(font1);
        groupBox_2->setStyleSheet(QLatin1String("#groupBox_2 {\n"
"border: 1px solid black;\n"
"}"));
        verticalLayout_5 = new QVBoxLayout(groupBox_2);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        label_12 = new QLabel(groupBox_2);
        label_12->setObjectName(QStringLiteral("label_12"));
        sizePolicy1.setHeightForWidth(label_12->sizePolicy().hasHeightForWidth());
        label_12->setSizePolicy(sizePolicy1);

        verticalLayout_5->addWidget(label_12);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        muaTaiLieuButton = new QPushButton(groupBox_2);
        muaTaiLieuButton->setObjectName(QStringLiteral("muaTaiLieuButton"));
        sizePolicy.setHeightForWidth(muaTaiLieuButton->sizePolicy().hasHeightForWidth());
        muaTaiLieuButton->setSizePolicy(sizePolicy);
        muaTaiLieuButton->setMinimumSize(QSize(20, 30));
        muaTaiLieuButton->setMaximumSize(QSize(250, 40));
        muaTaiLieuButton->setFont(font1);
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/images/request.png"), QSize(), QIcon::Normal, QIcon::Off);
        muaTaiLieuButton->setIcon(icon1);
        muaTaiLieuButton->setIconSize(QSize(42, 42));

        verticalLayout_4->addWidget(muaTaiLieuButton);

        gopYButton = new QPushButton(groupBox_2);
        gopYButton->setObjectName(QStringLiteral("gopYButton"));
        sizePolicy.setHeightForWidth(gopYButton->sizePolicy().hasHeightForWidth());
        gopYButton->setSizePolicy(sizePolicy);
        gopYButton->setMaximumSize(QSize(250, 40));
        gopYButton->setFont(font1);
        gopYButton->setLayoutDirection(Qt::LeftToRight);
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/images/feedback.png"), QSize(), QIcon::Normal, QIcon::Off);
        gopYButton->setIcon(icon2);
        gopYButton->setIconSize(QSize(28, 28));
        gopYButton->setAutoDefault(false);

        verticalLayout_4->addWidget(gopYButton);


        verticalLayout_5->addLayout(verticalLayout_4);


        verticalLayout_2->addWidget(groupBox_2);

        noiQuyButton = new QPushButton(LoginForm);
        noiQuyButton->setObjectName(QStringLiteral("noiQuyButton"));
        noiQuyButton->setFont(font4);

        verticalLayout_2->addWidget(noiQuyButton);


        horizontalLayout_5->addLayout(verticalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(100);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(30, -1, -1, -1);

        horizontalLayout_5->addLayout(horizontalLayout_3);

        groupBox_3 = new QGroupBox(LoginForm);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        QSizePolicy sizePolicy3(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(groupBox_3->sizePolicy().hasHeightForWidth());
        groupBox_3->setSizePolicy(sizePolicy3);
        groupBox_3->setFont(font1);
        groupBox_3->setStyleSheet(QLatin1String("#groupBox_3 {\n"
"border: 1px solid black;\n"
"}"));
        gridLayout_2 = new QGridLayout(groupBox_3);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        book = new QWidget(groupBox_3);
        book->setObjectName(QStringLiteral("book"));
        book->setStyleSheet(QStringLiteral(""));
        gridLayout = new QGridLayout(book);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        viewAllButton = new QPushButton(book);
        viewAllButton->setObjectName(QStringLiteral("viewAllButton"));
        QFont font5;
        font5.setPointSize(11);
        viewAllButton->setFont(font5);

        gridLayout->addWidget(viewAllButton, 17, 2, 1, 2);

        verticalSpacer = new QSpacerItem(121, 37, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 15, 2, 1, 1);

        searchButton = new QPushButton(book);
        searchButton->setObjectName(QStringLiteral("searchButton"));
        searchButton->setFont(font5);
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/images/search.png"), QSize(), QIcon::Normal, QIcon::Off);
        searchButton->setIcon(icon3);
        searchButton->setIconSize(QSize(24, 24));

        gridLayout->addWidget(searchButton, 0, 2, 1, 2);

        searchBox = new QLineEdit(book);
        searchBox->setObjectName(QStringLiteral("searchBox"));
        searchBox->setFont(font5);

        gridLayout->addWidget(searchBox, 0, 0, 1, 1);

        verticalSpacer_3 = new QSpacerItem(77, 37, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_3, 1, 2, 1, 1);

        label_7 = new QLabel(book);
        label_7->setObjectName(QStringLiteral("label_7"));
        QFont font6;
        font6.setPointSize(9);
        font6.setBold(false);
        font6.setWeight(50);
        label_7->setFont(font6);

        gridLayout->addWidget(label_7, 2, 2, 1, 1);

        label = new QLabel(book);
        label->setObjectName(QStringLiteral("label"));
        QFont font7;
        font7.setPointSize(9);
        label->setFont(font7);

        gridLayout->addWidget(label, 5, 2, 1, 1);

        verticalSpacer_7 = new QSpacerItem(77, 37, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_7, 12, 2, 1, 1);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_4, 4, 2, 1, 1);

        label_9 = new QLabel(book);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setFont(font7);

        gridLayout->addWidget(label_9, 10, 2, 1, 1);

        searchComboBox = new QComboBox(book);
        searchComboBox->setObjectName(QStringLiteral("searchComboBox"));
        QFont font8;
        font8.setPointSize(10);
        font8.setBold(false);
        font8.setWeight(50);
        searchComboBox->setFont(font8);

        gridLayout->addWidget(searchComboBox, 7, 2, 1, 2);

        label_10 = new QLabel(book);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setFont(font7);

        gridLayout->addWidget(label_10, 13, 2, 1, 1);

        sortModeComboBox = new QComboBox(book);
        sortModeComboBox->setObjectName(QStringLiteral("sortModeComboBox"));
        sortModeComboBox->setFont(font8);

        gridLayout->addWidget(sortModeComboBox, 14, 2, 1, 2);

        typeComboBox = new QComboBox(book);
        typeComboBox->setObjectName(QStringLiteral("typeComboBox"));
        typeComboBox->setFont(font8);

        gridLayout->addWidget(typeComboBox, 3, 2, 1, 2);

        sortComboBox = new QComboBox(book);
        sortComboBox->setObjectName(QStringLiteral("sortComboBox"));
        sortComboBox->setFont(font8);

        gridLayout->addWidget(sortComboBox, 11, 2, 1, 2);

        verticalSpacer_2 = new QSpacerItem(77, 37, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_2, 9, 2, 1, 1);

        bookTableView = new QTableView(book);
        bookTableView->setObjectName(QStringLiteral("bookTableView"));
        bookTableView->setToolTipDuration(-1);
        bookTableView->setStyleSheet(QStringLiteral(""));
        bookTableView->setEditTriggers(QAbstractItemView::AllEditTriggers);

        gridLayout->addWidget(bookTableView, 1, 0, 17, 1);


        verticalLayout->addWidget(book);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        viewMoreButton = new QPushButton(groupBox_3);
        viewMoreButton->setObjectName(QStringLiteral("viewMoreButton"));
        QSizePolicy sizePolicy4(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(viewMoreButton->sizePolicy().hasHeightForWidth());
        viewMoreButton->setSizePolicy(sizePolicy4);
        viewMoreButton->setMinimumSize(QSize(200, 0));
        viewMoreButton->setMaximumSize(QSize(300, 16777215));
        viewMoreButton->setFont(font1);
        viewMoreButton->setStyleSheet(QStringLiteral(""));
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/images/more.png"), QSize(), QIcon::Normal, QIcon::Off);
        viewMoreButton->setIcon(icon4);
        viewMoreButton->setIconSize(QSize(32, 32));

        horizontalLayout->addWidget(viewMoreButton);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        verticalLayout->addLayout(horizontalLayout);


        gridLayout_2->addLayout(verticalLayout, 0, 0, 1, 1);


        horizontalLayout_5->addWidget(groupBox_3);


        verticalLayout_6->addLayout(horizontalLayout_5);


        horizontalLayout_12->addLayout(verticalLayout_6);

        label_6 = new QLabel(LoginForm);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setStyleSheet(QStringLiteral("#label_6{border-image: url(:/images/images/Libpro.png)}"));

        horizontalLayout_12->addWidget(label_6);


        retranslateUi(LoginForm);

        loginButton->setDefault(false);


        QMetaObject::connectSlotsByName(LoginForm);
    } // setupUi

    void retranslateUi(QWidget *LoginForm)
    {
        LoginForm->setWindowTitle(QApplication::translate("LoginForm", "Ph\341\272\247n m\341\273\201m LibPro", Q_NULLPTR));
        label_8->setText(QString());
        label_5->setText(QApplication::translate("LoginForm", "LIBPRO", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("LoginForm", "\304\220\304\203ng nh\341\272\255p/\304\221\304\203ng k\303\275", Q_NULLPTR));
        label_2->setText(QString());
        label_11->setText(QString());
        label_3->setText(QApplication::translate("LoginForm", "T\303\252n t\303\240i kho\341\272\243n:", Q_NULLPTR));
        label_4->setText(QApplication::translate("LoginForm", "M\341\272\255t kh\341\272\251u:", Q_NULLPTR));
        userNameEdit->setText(QString());
        loginButton->setText(QApplication::translate("LoginForm", "\304\220\304\203ng nh\341\272\255p", Q_NULLPTR));
        signUpButton->setText(QApplication::translate("LoginForm", "\304\220\304\203ng k\303\275", Q_NULLPTR));
        groupBox_2->setTitle(QApplication::translate("LoginForm", "D\341\273\213ch v\341\273\245", Q_NULLPTR));
        label_12->setText(QString());
        muaTaiLieuButton->setText(QApplication::translate("LoginForm", "\304\220\341\273\201 ngh\341\273\213 mua t\303\240i li\341\273\207u", Q_NULLPTR));
        gopYButton->setText(QApplication::translate("LoginForm", "G\303\263p \303\275", Q_NULLPTR));
        noiQuyButton->setText(QApplication::translate("LoginForm", "N\341\273\231i quy", Q_NULLPTR));
        groupBox_3->setTitle(QApplication::translate("LoginForm", "T\303\254m ki\341\272\277m s\303\241ch", Q_NULLPTR));
        viewAllButton->setText(QApplication::translate("LoginForm", "Xem t\341\272\245t c\341\272\243", Q_NULLPTR));
        searchButton->setText(QApplication::translate("LoginForm", "T\303\254m ki\341\272\277m", Q_NULLPTR));
        searchBox->setText(QString());
        label_7->setText(QApplication::translate("LoginForm", "Danh m\341\273\245c", Q_NULLPTR));
        label->setText(QApplication::translate("LoginForm", "T\303\254m ki\341\272\277m theo", Q_NULLPTR));
        label_9->setText(QApplication::translate("LoginForm", "S\341\272\257p x\341\272\277p theo:", Q_NULLPTR));
        searchComboBox->clear();
        searchComboBox->insertItems(0, QStringList()
         << QApplication::translate("LoginForm", "T\341\272\245t c\341\272\243", Q_NULLPTR)
         << QApplication::translate("LoginForm", "M\303\243 s\303\241ch", Q_NULLPTR)
         << QApplication::translate("LoginForm", "T\303\252n s\303\241ch", Q_NULLPTR)
         << QApplication::translate("LoginForm", "Ch\341\273\247 \304\221\341\273\201", Q_NULLPTR)
         << QApplication::translate("LoginForm", "T\303\252n t\303\241c gi\341\272\243", Q_NULLPTR)
         << QApplication::translate("LoginForm", "Nh\303\240 xu\341\272\245t b\341\272\243n", Q_NULLPTR)
         << QApplication::translate("LoginForm", "N\304\203m xu\341\272\245t b\341\272\243n", Q_NULLPTR)
         << QApplication::translate("LoginForm", "S\341\273\221 s\303\241ch \304\221\303\243 m\306\260\341\273\243n", Q_NULLPTR)
         << QApplication::translate("LoginForm", "T\303\263m t\341\272\257t n\341\273\231i dung", Q_NULLPTR)
        );
        label_10->setText(QApplication::translate("LoginForm", "Th\341\273\251 t\341\273\261:", Q_NULLPTR));
        sortModeComboBox->clear();
        sortModeComboBox->insertItems(0, QStringList()
         << QApplication::translate("LoginForm", "T\304\203ng d\341\272\247n", Q_NULLPTR)
         << QApplication::translate("LoginForm", "Gi\341\272\243m d\341\272\247n", Q_NULLPTR)
        );
        typeComboBox->clear();
        typeComboBox->insertItems(0, QStringList()
         << QApplication::translate("LoginForm", "T\341\272\245t c\341\272\243", Q_NULLPTR)
         << QApplication::translate("LoginForm", "S\303\241ch", Q_NULLPTR)
         << QApplication::translate("LoginForm", "T\341\272\241p ch\303\255", Q_NULLPTR)
         << QApplication::translate("LoginForm", "Lu\341\272\255n \303\241n ti\341\272\277n s\304\251", Q_NULLPTR)
         << QApplication::translate("LoginForm", "Lu\341\272\255n v\304\203n th\341\272\241c s\304\251", Q_NULLPTR)
         << QApplication::translate("LoginForm", "Ti\303\252u chu\341\272\251n k\341\273\271 thu\341\272\255t", Q_NULLPTR)
         << QApplication::translate("LoginForm", "B\303\241o c\303\241o khoa h\341\273\215c", Q_NULLPTR)
        );
        sortComboBox->clear();
        sortComboBox->insertItems(0, QStringList()
         << QApplication::translate("LoginForm", "M\341\273\233i v\341\273\201", Q_NULLPTR)
         << QApplication::translate("LoginForm", "M\303\243 s\303\241ch", Q_NULLPTR)
         << QApplication::translate("LoginForm", "T\303\252n s\303\241ch", Q_NULLPTR)
         << QApplication::translate("LoginForm", "Ch\341\273\247 \304\221\341\273\201", Q_NULLPTR)
         << QApplication::translate("LoginForm", "T\303\252n t\303\241c gi\341\272\243", Q_NULLPTR)
         << QApplication::translate("LoginForm", "Nh\303\240 xu\341\272\245t b\341\272\243n", Q_NULLPTR)
         << QApplication::translate("LoginForm", "N\304\203m xu\341\272\245t b\341\272\243n", Q_NULLPTR)
         << QApplication::translate("LoginForm", "S\341\273\221 s\303\241ch \304\221\303\243 m\306\260\341\273\243n", Q_NULLPTR)
        );
        viewMoreButton->setText(QApplication::translate("LoginForm", "Xem th\303\252m", Q_NULLPTR));
        label_6->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class LoginForm: public Ui_LoginForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGINFORM_H
