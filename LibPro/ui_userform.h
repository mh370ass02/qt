/********************************************************************************
** Form generated from reading UI file 'userform.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_USERFORM_H
#define UI_USERFORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_UserForm
{
public:
    QPushButton *Button;
    QLabel *createLabel;
    QLabel *label_13;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QLineEdit *userName;
    QPushButton *passWordButton;
    QLineEdit *hoTen;
    QDateEdit *ngaySinh;
    QLineEdit *CMND;
    QLineEdit *MSSV;
    QComboBox *doiTuongComboBox;
    QWidget *layoutWidget1;
    QVBoxLayout *verticalLayout_3;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_9;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_12;
    QCheckBox *readerCheck;
    QCheckBox *userManaCheck;
    QCheckBox *librarianCheck;
    QLabel *label_6;
    QWidget *layoutWidget2;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_7;
    QLabel *label_10;
    QLabel *label_11;
    QLabel *label_8;
    QComboBox *khoa;
    QLineEdit *sdt;
    QLineEdit *khoaLineEdit;
    QLineEdit *email;
    QLineEdit *diaChi;
    QLineEdit *thongTin;

    void setupUi(QWidget *UserForm)
    {
        if (UserForm->objectName().isEmpty())
            UserForm->setObjectName(QStringLiteral("UserForm"));
        UserForm->resize(663, 388);
        UserForm->setMaximumSize(QSize(663, 388));
        Button = new QPushButton(UserForm);
        Button->setObjectName(QStringLiteral("Button"));
        Button->setGeometry(QRect(230, 340, 161, 31));
        QFont font;
        font.setPointSize(14);
        font.setBold(true);
        font.setWeight(75);
        Button->setFont(font);
        createLabel = new QLabel(UserForm);
        createLabel->setObjectName(QStringLiteral("createLabel"));
        createLabel->setGeometry(QRect(200, 10, 361, 41));
        QFont font1;
        font1.setPointSize(16);
        font1.setBold(true);
        font1.setWeight(75);
        createLabel->setFont(font1);
        label_13 = new QLabel(UserForm);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setGeometry(QRect(140, 10, 51, 41));
        label_13->setPixmap(QPixmap(QString::fromUtf8(":/images/user.png")));
        label_13->setScaledContents(true);
        layoutWidget = new QWidget(UserForm);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(116, 88, 211, 218));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        userName = new QLineEdit(layoutWidget);
        userName->setObjectName(QStringLiteral("userName"));
        QFont font2;
        font2.setPointSize(12);
        userName->setFont(font2);
        userName->setMaxLength(20);

        verticalLayout->addWidget(userName);

        passWordButton = new QPushButton(layoutWidget);
        passWordButton->setObjectName(QStringLiteral("passWordButton"));
        passWordButton->setMinimumSize(QSize(0, 30));
        QFont font3;
        font3.setPointSize(10);
        passWordButton->setFont(font3);

        verticalLayout->addWidget(passWordButton);

        hoTen = new QLineEdit(layoutWidget);
        hoTen->setObjectName(QStringLiteral("hoTen"));
        hoTen->setFont(font2);
        hoTen->setMaxLength(24);

        verticalLayout->addWidget(hoTen);

        ngaySinh = new QDateEdit(layoutWidget);
        ngaySinh->setObjectName(QStringLiteral("ngaySinh"));
        ngaySinh->setFont(font2);

        verticalLayout->addWidget(ngaySinh);

        CMND = new QLineEdit(layoutWidget);
        CMND->setObjectName(QStringLiteral("CMND"));
        CMND->setFont(font2);
        CMND->setInputMethodHints(Qt::ImhDigitsOnly);
        CMND->setMaxLength(12);

        verticalLayout->addWidget(CMND);

        MSSV = new QLineEdit(layoutWidget);
        MSSV->setObjectName(QStringLiteral("MSSV"));
        MSSV->setFont(font2);
        MSSV->setInputMethodHints(Qt::ImhDigitsOnly);

        verticalLayout->addWidget(MSSV);

        doiTuongComboBox = new QComboBox(layoutWidget);
        doiTuongComboBox->setObjectName(QStringLiteral("doiTuongComboBox"));
        doiTuongComboBox->setFont(font2);

        verticalLayout->addWidget(doiTuongComboBox);

        layoutWidget1 = new QWidget(UserForm);
        layoutWidget1->setObjectName(QStringLiteral("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(30, 88, 73, 221));
        verticalLayout_3 = new QVBoxLayout(layoutWidget1);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(layoutWidget1);
        label->setObjectName(QStringLiteral("label"));
        label->setFont(font2);

        verticalLayout_3->addWidget(label);

        label_2 = new QLabel(layoutWidget1);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setFont(font2);

        verticalLayout_3->addWidget(label_2);

        label_3 = new QLabel(layoutWidget1);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setFont(font2);

        verticalLayout_3->addWidget(label_3);

        label_9 = new QLabel(layoutWidget1);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setFont(font2);

        verticalLayout_3->addWidget(label_9);

        label_4 = new QLabel(layoutWidget1);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setFont(font2);

        verticalLayout_3->addWidget(label_4);

        label_5 = new QLabel(layoutWidget1);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setFont(font2);

        verticalLayout_3->addWidget(label_5);

        label_12 = new QLabel(layoutWidget1);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setFont(font2);

        verticalLayout_3->addWidget(label_12);

        readerCheck = new QCheckBox(UserForm);
        readerCheck->setObjectName(QStringLiteral("readerCheck"));
        readerCheck->setGeometry(QRect(411, 61, 76, 23));
        readerCheck->setFont(font2);
        readerCheck->setChecked(true);
        userManaCheck = new QCheckBox(UserForm);
        userManaCheck->setObjectName(QStringLiteral("userManaCheck"));
        userManaCheck->setGeometry(QRect(411, 90, 165, 23));
        userManaCheck->setFont(font2);
        librarianCheck = new QCheckBox(UserForm);
        librarianCheck->setObjectName(QStringLiteral("librarianCheck"));
        librarianCheck->setGeometry(QRect(509, 61, 81, 23));
        librarianCheck->setFont(font2);
        label_6 = new QLabel(UserForm);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(350, 120, 35, 19));
        label_6->setFont(font2);
        layoutWidget2 = new QWidget(UserForm);
        layoutWidget2->setObjectName(QStringLiteral("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(350, 180, 50, 121));
        verticalLayout_2 = new QVBoxLayout(layoutWidget2);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        label_7 = new QLabel(layoutWidget2);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setFont(font2);

        verticalLayout_2->addWidget(label_7);

        label_10 = new QLabel(layoutWidget2);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setFont(font2);

        verticalLayout_2->addWidget(label_10);

        label_11 = new QLabel(layoutWidget2);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setFont(font2);

        verticalLayout_2->addWidget(label_11);

        label_8 = new QLabel(layoutWidget2);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setFont(font2);

        verticalLayout_2->addWidget(label_8);

        khoa = new QComboBox(UserForm);
        khoa->setObjectName(QStringLiteral("khoa"));
        khoa->setGeometry(QRect(412, 120, 189, 24));
        QFont font4;
        font4.setPointSize(11);
        khoa->setFont(font4);
        sdt = new QLineEdit(UserForm);
        sdt->setObjectName(QStringLiteral("sdt"));
        sdt->setGeometry(QRect(412, 243, 191, 25));
        sdt->setFont(font2);
        sdt->setInputMethodHints(Qt::ImhDigitsOnly);
        khoaLineEdit = new QLineEdit(UserForm);
        khoaLineEdit->setObjectName(QStringLiteral("khoaLineEdit"));
        khoaLineEdit->setGeometry(QRect(412, 150, 191, 25));
        khoaLineEdit->setFont(font2);
        email = new QLineEdit(UserForm);
        email->setObjectName(QStringLiteral("email"));
        email->setGeometry(QRect(412, 212, 191, 25));
        email->setFont(font2);
        email->setInputMethodHints(Qt::ImhEmailCharactersOnly);
        diaChi = new QLineEdit(UserForm);
        diaChi->setObjectName(QStringLiteral("diaChi"));
        diaChi->setGeometry(QRect(412, 181, 191, 25));
        diaChi->setFont(font2);
        thongTin = new QLineEdit(UserForm);
        thongTin->setObjectName(QStringLiteral("thongTin"));
        thongTin->setGeometry(QRect(412, 274, 191, 25));
        thongTin->setFont(font2);

        retranslateUi(UserForm);

        QMetaObject::connectSlotsByName(UserForm);
    } // setupUi

    void retranslateUi(QWidget *UserForm)
    {
        UserForm->setWindowTitle(QApplication::translate("UserForm", "Ng\306\260\341\273\235i d\303\271ng", Q_NULLPTR));
        Button->setText(QApplication::translate("UserForm", "T\341\272\241o", Q_NULLPTR));
        createLabel->setText(QApplication::translate("UserForm", "T\341\272\241o ng\306\260\341\273\235i d\303\271ng m\341\273\233i", Q_NULLPTR));
        label_13->setText(QString());
        passWordButton->setText(QApplication::translate("UserForm", "T\341\272\241o m\341\272\255t kh\341\272\251u", Q_NULLPTR));
        hoTen->setText(QString());
        doiTuongComboBox->clear();
        doiTuongComboBox->insertItems(0, QStringList()
         << QApplication::translate("UserForm", "SV", Q_NULLPTR)
         << QApplication::translate("UserForm", "GV", Q_NULLPTR)
         << QApplication::translate("UserForm", "CNVC", Q_NULLPTR)
        );
        label->setText(QApplication::translate("UserForm", "T\303\240i kho\341\272\243n", Q_NULLPTR));
        label_2->setText(QApplication::translate("UserForm", "M\341\272\255t kh\341\272\251u", Q_NULLPTR));
        label_3->setText(QApplication::translate("UserForm", "H\341\273\215 v\303\240 t\303\252n", Q_NULLPTR));
        label_9->setText(QApplication::translate("UserForm", "Ng\303\240y sinh", Q_NULLPTR));
        label_4->setText(QApplication::translate("UserForm", "CMND", Q_NULLPTR));
        label_5->setText(QApplication::translate("UserForm", "MSSV/GV", Q_NULLPTR));
        label_12->setText(QApplication::translate("UserForm", "\304\220\341\273\221i t\306\260\341\273\243ng", Q_NULLPTR));
        readerCheck->setText(QApplication::translate("UserForm", "\304\220\341\273\231c gi\341\272\243", Q_NULLPTR));
        userManaCheck->setText(QApplication::translate("UserForm", "Qu\341\272\243n l\303\275 ng\306\260\341\273\235i d\303\271ng", Q_NULLPTR));
        librarianCheck->setText(QApplication::translate("UserForm", "Th\341\273\247 th\306\260", Q_NULLPTR));
        label_6->setText(QApplication::translate("UserForm", "Khoa", Q_NULLPTR));
        label_7->setText(QApplication::translate("UserForm", "\304\220\341\273\213a ch\341\273\211", Q_NULLPTR));
        label_10->setText(QApplication::translate("UserForm", "Email", Q_NULLPTR));
        label_11->setText(QApplication::translate("UserForm", "S\304\220T", Q_NULLPTR));
        label_8->setText(QApplication::translate("UserForm", "Kh\303\241c", Q_NULLPTR));
        khoa->clear();
        khoa->insertItems(0, QStringList()
         << QApplication::translate("UserForm", "KH&KT M\303\241y t\303\255nh", Q_NULLPTR)
         << QApplication::translate("UserForm", "\304\220i\341\273\207n - \304\220i\341\273\207n t\341\273\255", Q_NULLPTR)
         << QApplication::translate("UserForm", "KT X\303\242y d\341\273\261ng", Q_NULLPTR)
         << QApplication::translate("UserForm", "C\306\241 kh\303\255", Q_NULLPTR)
         << QApplication::translate("UserForm", "KT H\303\263a h\341\273\215c", Q_NULLPTR)
         << QApplication::translate("UserForm", "CN V\341\272\255t li\341\273\207u", Q_NULLPTR)
         << QApplication::translate("UserForm", "M\303\264i tr\306\260\341\273\235ng & T\303\240i nguy\303\252n", Q_NULLPTR)
         << QApplication::translate("UserForm", "Khoa h\341\273\215c \341\273\251ng d\341\273\245ng", Q_NULLPTR)
         << QApplication::translate("UserForm", "KT Giao th\303\264ng", Q_NULLPTR)
         << QApplication::translate("UserForm", "Qu\341\272\243n l\303\275 c\303\264ng nghi\341\273\207p", Q_NULLPTR)
         << QApplication::translate("UserForm", "KT & \304\220\341\273\213a ch\341\272\245t d\341\272\247u kh\303\255", Q_NULLPTR)
         << QApplication::translate("UserForm", "B\341\272\243o d\306\260\341\273\241ng CN", Q_NULLPTR)
         << QApplication::translate("UserForm", "KS CLC PFIEV", Q_NULLPTR)
         << QApplication::translate("UserForm", "Li\303\252n k\341\272\277t Qu\341\273\221c t\341\272\277 OISP", Q_NULLPTR)
         << QApplication::translate("UserForm", "Kh\303\241c", Q_NULLPTR)
        );
        diaChi->setText(QString());
        thongTin->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class UserForm: public Ui_UserForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_USERFORM_H
