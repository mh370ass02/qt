#ifndef ADDUSERFORM_H
#define ADDUSERFORM_H

#include <QWidget>
#include <QCloseEvent>
#include <QSqlQuery>

namespace Ui {
class AddUserForm;
}

class AddUserForm : public QWidget
{
    Q_OBJECT

public:
    explicit AddUserForm(QWidget *parent = 0, int mode = 0);
    ~AddUserForm();
    void closeEvent (QCloseEvent *event);
    void start();

private slots:
    void on_createButton_clicked();

private:
    Ui::AddUserForm *ui;
    QSqlQuery query;

};

#endif // ADDUSERFORM_H