/********************************************************************************
** Form generated from reading UI file 'notiform.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NOTIFORM_H
#define UI_NOTIFORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_NotiForm
{
public:
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_4;
    QLabel *label;
    QSpacerItem *horizontalSpacer_3;
    QGridLayout *gridLayout;
    QLabel *label_5;
    QLineEdit *fromEdit;
    QLabel *label_2;
    QLineEdit *toEdit;
    QLabel *label_3;
    QLineEdit *titleEdit;
    QLabel *label_6;
    QDateEdit *dateEdit;
    QLabel *label_4;
    QTextEdit *bodyEdit;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QPushButton *button;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label_7;

    void setupUi(QWidget *NotiForm)
    {
        if (NotiForm->objectName().isEmpty())
            NotiForm->setObjectName(QStringLiteral("NotiForm"));
        NotiForm->resize(600, 400);
        NotiForm->setMinimumSize(QSize(400, 250));
        horizontalLayout_3 = new QHBoxLayout(NotiForm);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_4);

        label = new QLabel(NotiForm);
        label->setObjectName(QStringLiteral("label"));
        QFont font;
        font.setPointSize(16);
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);

        horizontalLayout->addWidget(label);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_3);


        verticalLayout->addLayout(horizontalLayout);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label_5 = new QLabel(NotiForm);
        label_5->setObjectName(QStringLiteral("label_5"));
        QFont font1;
        font1.setPointSize(11);
        label_5->setFont(font1);

        gridLayout->addWidget(label_5, 0, 0, 1, 1);

        fromEdit = new QLineEdit(NotiForm);
        fromEdit->setObjectName(QStringLiteral("fromEdit"));
        fromEdit->setFont(font1);

        gridLayout->addWidget(fromEdit, 0, 1, 1, 1);

        label_2 = new QLabel(NotiForm);
        label_2->setObjectName(QStringLiteral("label_2"));
        QFont font2;
        font2.setPointSize(12);
        label_2->setFont(font2);

        gridLayout->addWidget(label_2, 0, 2, 1, 1);

        toEdit = new QLineEdit(NotiForm);
        toEdit->setObjectName(QStringLiteral("toEdit"));
        toEdit->setFont(font1);

        gridLayout->addWidget(toEdit, 0, 3, 1, 1);

        label_3 = new QLabel(NotiForm);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setFont(font1);

        gridLayout->addWidget(label_3, 1, 0, 1, 1);

        titleEdit = new QLineEdit(NotiForm);
        titleEdit->setObjectName(QStringLiteral("titleEdit"));
        titleEdit->setFont(font1);

        gridLayout->addWidget(titleEdit, 1, 1, 1, 1);

        label_6 = new QLabel(NotiForm);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setFont(font2);

        gridLayout->addWidget(label_6, 1, 2, 1, 1);

        dateEdit = new QDateEdit(NotiForm);
        dateEdit->setObjectName(QStringLiteral("dateEdit"));
        dateEdit->setFont(font1);

        gridLayout->addWidget(dateEdit, 1, 3, 1, 1);

        label_4 = new QLabel(NotiForm);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setFont(font1);

        gridLayout->addWidget(label_4, 2, 0, 1, 1);

        bodyEdit = new QTextEdit(NotiForm);
        bodyEdit->setObjectName(QStringLiteral("bodyEdit"));
        bodyEdit->setFont(font1);

        gridLayout->addWidget(bodyEdit, 2, 1, 1, 3);


        verticalLayout->addLayout(gridLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        button = new QPushButton(NotiForm);
        button->setObjectName(QStringLiteral("button"));
        QFont font3;
        font3.setPointSize(11);
        font3.setBold(true);
        font3.setWeight(75);
        button->setFont(font3);

        horizontalLayout_2->addWidget(button);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout_2);

        label_7 = new QLabel(NotiForm);
        label_7->setObjectName(QStringLiteral("label_7"));
        QFont font4;
        font4.setPointSize(10);
        label_7->setFont(font4);

        verticalLayout->addWidget(label_7);


        horizontalLayout_3->addLayout(verticalLayout);


        retranslateUi(NotiForm);

        QMetaObject::connectSlotsByName(NotiForm);
    } // setupUi

    void retranslateUi(QWidget *NotiForm)
    {
        NotiForm->setWindowTitle(QApplication::translate("NotiForm", "Th\303\264ng b\303\241o", Q_NULLPTR));
        label->setText(QApplication::translate("NotiForm", "Th\303\264ng b\303\241o", Q_NULLPTR));
        label_5->setText(QApplication::translate("NotiForm", "T\341\273\253", Q_NULLPTR));
        label_2->setText(QApplication::translate("NotiForm", "T\341\273\233i *", Q_NULLPTR));
        label_3->setText(QApplication::translate("NotiForm", "Ti\303\252u \304\221\341\273\201", Q_NULLPTR));
        label_6->setText(QString());
        label_4->setText(QApplication::translate("NotiForm", "N\341\273\231i dung", Q_NULLPTR));
        button->setText(QApplication::translate("NotiForm", "Button", Q_NULLPTR));
        label_7->setText(QApplication::translate("NotiForm", "* \304\220\341\273\203 tr\341\273\221ng \304\221\341\273\203 g\341\273\255i t\341\273\233i t\341\272\245t c\341\272\243", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class NotiForm: public Ui_NotiForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NOTIFORM_H
