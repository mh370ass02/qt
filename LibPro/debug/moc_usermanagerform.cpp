/****************************************************************************
** Meta object code from reading C++ file 'usermanagerform.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../usermanagerform.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'usermanagerform.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_UserManagerForm_t {
    QByteArrayData data[16];
    char stringdata0[357];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_UserManagerForm_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_UserManagerForm_t qt_meta_stringdata_UserManagerForm = {
    {
QT_MOC_LITERAL(0, 0, 15), // "UserManagerForm"
QT_MOC_LITERAL(1, 16, 34), // "on_roleComboBox_currentTextCh..."
QT_MOC_LITERAL(2, 51, 0), // ""
QT_MOC_LITERAL(3, 52, 4), // "arg1"
QT_MOC_LITERAL(4, 57, 21), // "on_logoutForm_clicked"
QT_MOC_LITERAL(5, 79, 15), // "on_user_clicked"
QT_MOC_LITERAL(6, 95, 27), // "on_createUserButton_clicked"
QT_MOC_LITERAL(7, 123, 25), // "on_editUserButton_clicked"
QT_MOC_LITERAL(8, 149, 27), // "on_deleteUserButton_clicked"
QT_MOC_LITERAL(9, 177, 37), // "on_searchComboBox_currentInde..."
QT_MOC_LITERAL(10, 215, 5), // "index"
QT_MOC_LITERAL(11, 221, 35), // "on_typeComboBox_currentIndexC..."
QT_MOC_LITERAL(12, 257, 27), // "on_searchUserButton_clicked"
QT_MOC_LITERAL(13, 285, 24), // "on_viewAllButton_clicked"
QT_MOC_LITERAL(14, 310, 24), // "on_userTableView_pressed"
QT_MOC_LITERAL(15, 335, 21) // "on_lockButton_clicked"

    },
    "UserManagerForm\0on_roleComboBox_currentTextChanged\0"
    "\0arg1\0on_logoutForm_clicked\0on_user_clicked\0"
    "on_createUserButton_clicked\0"
    "on_editUserButton_clicked\0"
    "on_deleteUserButton_clicked\0"
    "on_searchComboBox_currentIndexChanged\0"
    "index\0on_typeComboBox_currentIndexChanged\0"
    "on_searchUserButton_clicked\0"
    "on_viewAllButton_clicked\0"
    "on_userTableView_pressed\0on_lockButton_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_UserManagerForm[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   74,    2, 0x08 /* Private */,
       4,    0,   77,    2, 0x08 /* Private */,
       5,    0,   78,    2, 0x08 /* Private */,
       6,    0,   79,    2, 0x08 /* Private */,
       7,    0,   80,    2, 0x08 /* Private */,
       8,    0,   81,    2, 0x08 /* Private */,
       9,    1,   82,    2, 0x08 /* Private */,
      11,    1,   85,    2, 0x08 /* Private */,
      12,    0,   88,    2, 0x08 /* Private */,
      13,    0,   89,    2, 0x08 /* Private */,
      14,    1,   90,    2, 0x08 /* Private */,
      15,    0,   93,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,   10,
    QMetaType::Void,

       0        // eod
};

void UserManagerForm::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        UserManagerForm *_t = static_cast<UserManagerForm *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_roleComboBox_currentTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->on_logoutForm_clicked(); break;
        case 2: _t->on_user_clicked(); break;
        case 3: _t->on_createUserButton_clicked(); break;
        case 4: _t->on_editUserButton_clicked(); break;
        case 5: _t->on_deleteUserButton_clicked(); break;
        case 6: _t->on_searchComboBox_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->on_typeComboBox_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->on_searchUserButton_clicked(); break;
        case 9: _t->on_viewAllButton_clicked(); break;
        case 10: _t->on_userTableView_pressed((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 11: _t->on_lockButton_clicked(); break;
        default: ;
        }
    }
}

const QMetaObject UserManagerForm::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_UserManagerForm.data,
      qt_meta_data_UserManagerForm,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *UserManagerForm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *UserManagerForm::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_UserManagerForm.stringdata0))
        return static_cast<void*>(const_cast< UserManagerForm*>(this));
    return QWidget::qt_metacast(_clname);
}

int UserManagerForm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 12)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 12;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
