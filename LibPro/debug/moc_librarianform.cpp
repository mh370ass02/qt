/****************************************************************************
** Meta object code from reading C++ file 'librarianform.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../librarianform.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'librarianform.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_LibrarianForm_t {
    QByteArrayData data[23];
    char stringdata0[600];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_LibrarianForm_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_LibrarianForm_t qt_meta_stringdata_LibrarianForm = {
    {
QT_MOC_LITERAL(0, 0, 13), // "LibrarianForm"
QT_MOC_LITERAL(1, 14, 34), // "on_roleComboBox_currentTextCh..."
QT_MOC_LITERAL(2, 49, 0), // ""
QT_MOC_LITERAL(3, 50, 4), // "arg1"
QT_MOC_LITERAL(4, 55, 23), // "on_logoutButton_clicked"
QT_MOC_LITERAL(5, 79, 27), // "on_deleteBookButton_clicked"
QT_MOC_LITERAL(6, 107, 23), // "on_searchButton_clicked"
QT_MOC_LITERAL(7, 131, 24), // "on_viewAllButton_clicked"
QT_MOC_LITERAL(8, 156, 35), // "on_sortComboBox_currentIndexC..."
QT_MOC_LITERAL(9, 192, 5), // "index"
QT_MOC_LITERAL(10, 198, 39), // "on_sortModeComboBox_currentIn..."
QT_MOC_LITERAL(11, 238, 25), // "on_viewMoreButton_clicked"
QT_MOC_LITERAL(12, 264, 24), // "on_addBookButton_clicked"
QT_MOC_LITERAL(13, 289, 15), // "on_user_clicked"
QT_MOC_LITERAL(14, 305, 35), // "on_typeComboBox_currentIndexC..."
QT_MOC_LITERAL(15, 341, 37), // "on_searchComboBox_currentInde..."
QT_MOC_LITERAL(16, 379, 31), // "on_viewAllRequestButton_clicked"
QT_MOC_LITERAL(17, 411, 30), // "on_searchRequestButton_clicked"
QT_MOC_LITERAL(18, 442, 44), // "on_searchRequestComboBox_curr..."
QT_MOC_LITERAL(19, 487, 42), // "on_typeRequestComboBox_curren..."
QT_MOC_LITERAL(20, 530, 17), // "on_button_clicked"
QT_MOC_LITERAL(21, 548, 27), // "on_requestTableView_pressed"
QT_MOC_LITERAL(22, 576, 23) // "on_deleteButton_clicked"

    },
    "LibrarianForm\0on_roleComboBox_currentTextChanged\0"
    "\0arg1\0on_logoutButton_clicked\0"
    "on_deleteBookButton_clicked\0"
    "on_searchButton_clicked\0"
    "on_viewAllButton_clicked\0"
    "on_sortComboBox_currentIndexChanged\0"
    "index\0on_sortModeComboBox_currentIndexChanged\0"
    "on_viewMoreButton_clicked\0"
    "on_addBookButton_clicked\0on_user_clicked\0"
    "on_typeComboBox_currentIndexChanged\0"
    "on_searchComboBox_currentIndexChanged\0"
    "on_viewAllRequestButton_clicked\0"
    "on_searchRequestButton_clicked\0"
    "on_searchRequestComboBox_currentIndexChanged\0"
    "on_typeRequestComboBox_currentIndexChanged\0"
    "on_button_clicked\0on_requestTableView_pressed\0"
    "on_deleteButton_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_LibrarianForm[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,  109,    2, 0x08 /* Private */,
       4,    0,  112,    2, 0x08 /* Private */,
       5,    0,  113,    2, 0x08 /* Private */,
       6,    0,  114,    2, 0x08 /* Private */,
       7,    0,  115,    2, 0x08 /* Private */,
       8,    1,  116,    2, 0x08 /* Private */,
      10,    1,  119,    2, 0x08 /* Private */,
      11,    0,  122,    2, 0x08 /* Private */,
      12,    0,  123,    2, 0x08 /* Private */,
      13,    0,  124,    2, 0x08 /* Private */,
      14,    1,  125,    2, 0x08 /* Private */,
      15,    1,  128,    2, 0x08 /* Private */,
      16,    0,  131,    2, 0x08 /* Private */,
      17,    0,  132,    2, 0x08 /* Private */,
      18,    1,  133,    2, 0x08 /* Private */,
      19,    1,  136,    2, 0x08 /* Private */,
      20,    0,  139,    2, 0x08 /* Private */,
      21,    1,  140,    2, 0x08 /* Private */,
      22,    0,  143,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,    9,
    QMetaType::Void,

       0        // eod
};

void LibrarianForm::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        LibrarianForm *_t = static_cast<LibrarianForm *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_roleComboBox_currentTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->on_logoutButton_clicked(); break;
        case 2: _t->on_deleteBookButton_clicked(); break;
        case 3: _t->on_searchButton_clicked(); break;
        case 4: _t->on_viewAllButton_clicked(); break;
        case 5: _t->on_sortComboBox_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->on_sortModeComboBox_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->on_viewMoreButton_clicked(); break;
        case 8: _t->on_addBookButton_clicked(); break;
        case 9: _t->on_user_clicked(); break;
        case 10: _t->on_typeComboBox_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->on_searchComboBox_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->on_viewAllRequestButton_clicked(); break;
        case 13: _t->on_searchRequestButton_clicked(); break;
        case 14: _t->on_searchRequestComboBox_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 15: _t->on_typeRequestComboBox_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 16: _t->on_button_clicked(); break;
        case 17: _t->on_requestTableView_pressed((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 18: _t->on_deleteButton_clicked(); break;
        default: ;
        }
    }
}

const QMetaObject LibrarianForm::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_LibrarianForm.data,
      qt_meta_data_LibrarianForm,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *LibrarianForm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *LibrarianForm::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_LibrarianForm.stringdata0))
        return static_cast<void*>(const_cast< LibrarianForm*>(this));
    return QWidget::qt_metacast(_clname);
}

int LibrarianForm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 19)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 19;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
