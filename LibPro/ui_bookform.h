/********************************************************************************
** Form generated from reading UI file 'bookform.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BOOKFORM_H
#define UI_BOOKFORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_BookForm
{
public:
    QGridLayout *gridLayout_5;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_5;
    QLabel *label;
    QSpacerItem *horizontalSpacer_6;
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout_4;
    QLineEdit *maSachEdit;
    QSplitter *splitter;
    QLabel *label_2;
    QLabel *label_16;
    QLabel *label_3;
    QLabel *label_9;
    QLabel *label_12;
    QLabel *label_4;
    QLabel *label_5;
    QGridLayout *gridLayout_3;
    QSpacerItem *horizontalSpacer;
    QLabel *label_11;
    QLabel *label_14;
    QLabel *label_10;
    QGridLayout *gridLayout;
    QLineEdit *namXuatBanEdit;
    QLineEdit *chuDeEdit;
    QComboBox *danhMucComboBox;
    QCheckBox *checkBox;
    QGridLayout *gridLayout_2;
    QLineEdit *tongCongEdit;
    QLineEdit *giaSachEdit;
    QLineEdit *daMuonEdit;
    QLineEdit *tenTacGiaEdit;
    QLineEdit *tenSachEdit;
    QLineEdit *nhaXuatBanEdit;
    QSpacerItem *horizontalSpacer_4;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label_8;
    QSpacerItem *horizontalSpacer_3;
    QTextEdit *tomTatEdit;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_8;
    QPushButton *button;
    QSpacerItem *horizontalSpacer_7;

    void setupUi(QWidget *BookForm)
    {
        if (BookForm->objectName().isEmpty())
            BookForm->setObjectName(QStringLiteral("BookForm"));
        BookForm->resize(662, 542);
        gridLayout_5 = new QGridLayout(BookForm);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_5);

        label = new QLabel(BookForm);
        label->setObjectName(QStringLiteral("label"));
        QFont font;
        font.setPointSize(16);
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);

        horizontalLayout_2->addWidget(label);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_6);


        verticalLayout_2->addLayout(horizontalLayout_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        gridLayout_4 = new QGridLayout();
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        maSachEdit = new QLineEdit(BookForm);
        maSachEdit->setObjectName(QStringLiteral("maSachEdit"));
        QFont font1;
        font1.setPointSize(11);
        maSachEdit->setFont(font1);

        gridLayout_4->addWidget(maSachEdit, 1, 1, 1, 1);

        splitter = new QSplitter(BookForm);
        splitter->setObjectName(QStringLiteral("splitter"));
        splitter->setFont(font1);
        splitter->setOrientation(Qt::Vertical);
        label_2 = new QLabel(splitter);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setFont(font1);
        splitter->addWidget(label_2);
        label_16 = new QLabel(splitter);
        label_16->setObjectName(QStringLiteral("label_16"));
        label_16->setFont(font1);
        splitter->addWidget(label_16);
        label_3 = new QLabel(splitter);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setFont(font1);
        splitter->addWidget(label_3);
        label_9 = new QLabel(splitter);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setFont(font1);
        splitter->addWidget(label_9);
        label_12 = new QLabel(splitter);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setFont(font1);
        splitter->addWidget(label_12);
        label_4 = new QLabel(splitter);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setFont(font1);
        splitter->addWidget(label_4);
        label_5 = new QLabel(splitter);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setFont(font1);
        splitter->addWidget(label_5);

        gridLayout_4->addWidget(splitter, 0, 0, 5, 1);

        gridLayout_3 = new QGridLayout();
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        horizontalSpacer = new QSpacerItem(118, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer, 0, 0, 1, 1);

        label_11 = new QLabel(BookForm);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setFont(font1);

        gridLayout_3->addWidget(label_11, 3, 0, 1, 1);

        label_14 = new QLabel(BookForm);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setFont(font1);

        gridLayout_3->addWidget(label_14, 2, 0, 1, 1);

        label_10 = new QLabel(BookForm);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setFont(font1);

        gridLayout_3->addWidget(label_10, 1, 0, 1, 1);


        gridLayout_4->addLayout(gridLayout_3, 1, 3, 2, 1);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        namXuatBanEdit = new QLineEdit(BookForm);
        namXuatBanEdit->setObjectName(QStringLiteral("namXuatBanEdit"));
        namXuatBanEdit->setFont(font1);

        gridLayout->addWidget(namXuatBanEdit, 2, 0, 1, 1);

        chuDeEdit = new QLineEdit(BookForm);
        chuDeEdit->setObjectName(QStringLiteral("chuDeEdit"));
        chuDeEdit->setFont(font1);

        gridLayout->addWidget(chuDeEdit, 1, 0, 1, 1);

        danhMucComboBox = new QComboBox(BookForm);
        danhMucComboBox->setObjectName(QStringLiteral("danhMucComboBox"));
        danhMucComboBox->setFont(font1);

        gridLayout->addWidget(danhMucComboBox, 0, 0, 1, 1);


        gridLayout_4->addLayout(gridLayout, 2, 1, 1, 1);

        checkBox = new QCheckBox(BookForm);
        checkBox->setObjectName(QStringLiteral("checkBox"));
        checkBox->setFont(font1);

        gridLayout_4->addWidget(checkBox, 1, 4, 1, 1);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        tongCongEdit = new QLineEdit(BookForm);
        tongCongEdit->setObjectName(QStringLiteral("tongCongEdit"));
        tongCongEdit->setFont(font1);

        gridLayout_2->addWidget(tongCongEdit, 1, 0, 1, 1);

        giaSachEdit = new QLineEdit(BookForm);
        giaSachEdit->setObjectName(QStringLiteral("giaSachEdit"));
        giaSachEdit->setFont(font1);

        gridLayout_2->addWidget(giaSachEdit, 2, 0, 1, 1);

        daMuonEdit = new QLineEdit(BookForm);
        daMuonEdit->setObjectName(QStringLiteral("daMuonEdit"));
        daMuonEdit->setFont(font1);

        gridLayout_2->addWidget(daMuonEdit, 0, 0, 1, 1);


        gridLayout_4->addLayout(gridLayout_2, 2, 4, 1, 1);

        tenTacGiaEdit = new QLineEdit(BookForm);
        tenTacGiaEdit->setObjectName(QStringLiteral("tenTacGiaEdit"));
        tenTacGiaEdit->setFont(font1);

        gridLayout_4->addWidget(tenTacGiaEdit, 3, 1, 1, 4);

        tenSachEdit = new QLineEdit(BookForm);
        tenSachEdit->setObjectName(QStringLiteral("tenSachEdit"));
        tenSachEdit->setFont(font1);
        tenSachEdit->setEchoMode(QLineEdit::Normal);

        gridLayout_4->addWidget(tenSachEdit, 0, 1, 1, 4);

        nhaXuatBanEdit = new QLineEdit(BookForm);
        nhaXuatBanEdit->setObjectName(QStringLiteral("nhaXuatBanEdit"));
        nhaXuatBanEdit->setFont(font1);

        gridLayout_4->addWidget(nhaXuatBanEdit, 4, 1, 1, 4);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_4->addItem(horizontalSpacer_4, 2, 2, 1, 1);


        verticalLayout->addLayout(gridLayout_4);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        label_8 = new QLabel(BookForm);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setFont(font1);

        horizontalLayout->addWidget(label_8);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_3);


        verticalLayout->addLayout(horizontalLayout);

        tomTatEdit = new QTextEdit(BookForm);
        tomTatEdit->setObjectName(QStringLiteral("tomTatEdit"));
        tomTatEdit->setFont(font1);

        verticalLayout->addWidget(tomTatEdit);


        verticalLayout_2->addLayout(verticalLayout);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_8);

        button = new QPushButton(BookForm);
        button->setObjectName(QStringLiteral("button"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(button->sizePolicy().hasHeightForWidth());
        button->setSizePolicy(sizePolicy);
        button->setFont(font1);
        QIcon icon;
        icon.addFile(QStringLiteral(":/images/booktick.png"), QSize(), QIcon::Normal, QIcon::Off);
        button->setIcon(icon);
        button->setIconSize(QSize(32, 32));

        horizontalLayout_3->addWidget(button);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_7);


        verticalLayout_2->addLayout(horizontalLayout_3);


        gridLayout_5->addLayout(verticalLayout_2, 0, 0, 1, 1);


        retranslateUi(BookForm);

        QMetaObject::connectSlotsByName(BookForm);
    } // setupUi

    void retranslateUi(QWidget *BookForm)
    {
        BookForm->setWindowTitle(QApplication::translate("BookForm", "S\303\241ch", Q_NULLPTR));
        label->setText(QApplication::translate("BookForm", "Th\303\252m s\303\241ch", Q_NULLPTR));
        label_2->setText(QApplication::translate("BookForm", "T\303\252n s\303\241ch", Q_NULLPTR));
        label_16->setText(QApplication::translate("BookForm", "M\303\243 s\303\241ch", Q_NULLPTR));
        label_3->setText(QApplication::translate("BookForm", "Danh m\341\273\245c", Q_NULLPTR));
        label_9->setText(QApplication::translate("BookForm", "Ch\341\273\247 \304\221\341\273\201", Q_NULLPTR));
        label_12->setText(QApplication::translate("BookForm", "N\304\203m xu\341\272\245t b\341\272\243n", Q_NULLPTR));
        label_4->setText(QApplication::translate("BookForm", "T\303\252n t\303\241c gi\341\272\243", Q_NULLPTR));
        label_5->setText(QApplication::translate("BookForm", "Nh\303\240 xu\341\272\245t b\341\272\243n", Q_NULLPTR));
        label_11->setText(QApplication::translate("BookForm", "Gi\303\241 s\303\241ch", Q_NULLPTR));
        label_14->setText(QApplication::translate("BookForm", "T\341\273\225ng c\341\273\231ng", Q_NULLPTR));
        label_10->setText(QApplication::translate("BookForm", "\304\220\303\243 m\306\260\341\273\243n", Q_NULLPTR));
        danhMucComboBox->clear();
        danhMucComboBox->insertItems(0, QStringList()
         << QApplication::translate("BookForm", "S\303\241ch", Q_NULLPTR)
         << QApplication::translate("BookForm", "T\341\272\241p ch\303\255", Q_NULLPTR)
         << QApplication::translate("BookForm", "Lu\341\272\255n v\304\203n ti\341\272\277n s\304\251", Q_NULLPTR)
         << QApplication::translate("BookForm", "Lu\341\272\255n v\304\203n th\341\272\241c s\304\251", Q_NULLPTR)
         << QApplication::translate("BookForm", "Ti\303\252u chu\341\272\251n k\341\273\271 thu\341\272\255t", Q_NULLPTR)
         << QApplication::translate("BookForm", "B\303\241o c\303\241o khoa h\341\273\215c", Q_NULLPTR)
        );
        checkBox->setText(QApplication::translate("BookForm", "\304\220\306\260\341\273\243c ph\303\251p m\306\260\341\273\243n", Q_NULLPTR));
        tongCongEdit->setText(QString());
        label_8->setText(QApplication::translate("BookForm", "T\303\263m t\341\272\257t n\341\273\231i dung", Q_NULLPTR));
        tomTatEdit->setHtml(QApplication::translate("BookForm", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8.25pt;\"><br /></p></body></html>", Q_NULLPTR));
        button->setText(QApplication::translate("BookForm", "C\341\272\255p nh\341\272\255t", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class BookForm: public Ui_BookForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BOOKFORM_H
