/****************************************************************************
** Meta object code from reading C++ file 'readerform.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../readerform.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'readerform.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ReaderForm_t {
    QByteArrayData data[24];
    char stringdata0[587];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ReaderForm_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ReaderForm_t qt_meta_stringdata_ReaderForm = {
    {
QT_MOC_LITERAL(0, 0, 10), // "ReaderForm"
QT_MOC_LITERAL(1, 11, 34), // "on_roleComboBox_currentTextCh..."
QT_MOC_LITERAL(2, 46, 0), // ""
QT_MOC_LITERAL(3, 47, 4), // "arg1"
QT_MOC_LITERAL(4, 52, 21), // "on_logoutForm_clicked"
QT_MOC_LITERAL(5, 74, 15), // "on_user_clicked"
QT_MOC_LITERAL(6, 90, 23), // "on_searchButton_clicked"
QT_MOC_LITERAL(7, 114, 24), // "on_viewAllButton_clicked"
QT_MOC_LITERAL(8, 139, 35), // "on_sortComboBox_currentIndexC..."
QT_MOC_LITERAL(9, 175, 5), // "index"
QT_MOC_LITERAL(10, 181, 39), // "on_sortModeComboBox_currentIn..."
QT_MOC_LITERAL(11, 221, 23), // "on_borrowButton_clicked"
QT_MOC_LITERAL(12, 245, 25), // "on_viewMoreButton_clicked"
QT_MOC_LITERAL(13, 271, 27), // "on_deleteCartButton_clicked"
QT_MOC_LITERAL(14, 299, 24), // "on_requestButton_clicked"
QT_MOC_LITERAL(15, 324, 35), // "on_typeComboBox_currentIndexC..."
QT_MOC_LITERAL(16, 360, 37), // "on_searchComboBox_currentInde..."
QT_MOC_LITERAL(17, 398, 30), // "on_deleteRequestButton_clicked"
QT_MOC_LITERAL(18, 429, 25), // "on_viewNotiButton_clicked"
QT_MOC_LITERAL(19, 455, 27), // "on_deleteNotiButton_clicked"
QT_MOC_LITERAL(20, 483, 22), // "on_hoTroButton_clicked"
QT_MOC_LITERAL(21, 506, 21), // "on_gopYButton_clicked"
QT_MOC_LITERAL(22, 528, 30), // "on_deNghiMuaSachButton_clicked"
QT_MOC_LITERAL(23, 559, 27) // "on_searchNotiButton_clicked"

    },
    "ReaderForm\0on_roleComboBox_currentTextChanged\0"
    "\0arg1\0on_logoutForm_clicked\0on_user_clicked\0"
    "on_searchButton_clicked\0"
    "on_viewAllButton_clicked\0"
    "on_sortComboBox_currentIndexChanged\0"
    "index\0on_sortModeComboBox_currentIndexChanged\0"
    "on_borrowButton_clicked\0"
    "on_viewMoreButton_clicked\0"
    "on_deleteCartButton_clicked\0"
    "on_requestButton_clicked\0"
    "on_typeComboBox_currentIndexChanged\0"
    "on_searchComboBox_currentIndexChanged\0"
    "on_deleteRequestButton_clicked\0"
    "on_viewNotiButton_clicked\0"
    "on_deleteNotiButton_clicked\0"
    "on_hoTroButton_clicked\0on_gopYButton_clicked\0"
    "on_deNghiMuaSachButton_clicked\0"
    "on_searchNotiButton_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ReaderForm[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      20,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,  114,    2, 0x08 /* Private */,
       4,    0,  117,    2, 0x08 /* Private */,
       5,    0,  118,    2, 0x08 /* Private */,
       6,    0,  119,    2, 0x08 /* Private */,
       7,    0,  120,    2, 0x08 /* Private */,
       8,    1,  121,    2, 0x08 /* Private */,
      10,    1,  124,    2, 0x08 /* Private */,
      11,    0,  127,    2, 0x08 /* Private */,
      12,    0,  128,    2, 0x08 /* Private */,
      13,    0,  129,    2, 0x08 /* Private */,
      14,    0,  130,    2, 0x08 /* Private */,
      15,    1,  131,    2, 0x08 /* Private */,
      16,    1,  134,    2, 0x08 /* Private */,
      17,    0,  137,    2, 0x08 /* Private */,
      18,    0,  138,    2, 0x08 /* Private */,
      19,    0,  139,    2, 0x08 /* Private */,
      20,    0,  140,    2, 0x08 /* Private */,
      21,    0,  141,    2, 0x08 /* Private */,
      22,    0,  142,    2, 0x08 /* Private */,
      23,    0,  143,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void ReaderForm::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ReaderForm *_t = static_cast<ReaderForm *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_roleComboBox_currentTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->on_logoutForm_clicked(); break;
        case 2: _t->on_user_clicked(); break;
        case 3: _t->on_searchButton_clicked(); break;
        case 4: _t->on_viewAllButton_clicked(); break;
        case 5: _t->on_sortComboBox_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->on_sortModeComboBox_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->on_borrowButton_clicked(); break;
        case 8: _t->on_viewMoreButton_clicked(); break;
        case 9: _t->on_deleteCartButton_clicked(); break;
        case 10: _t->on_requestButton_clicked(); break;
        case 11: _t->on_typeComboBox_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->on_searchComboBox_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->on_deleteRequestButton_clicked(); break;
        case 14: _t->on_viewNotiButton_clicked(); break;
        case 15: _t->on_deleteNotiButton_clicked(); break;
        case 16: _t->on_hoTroButton_clicked(); break;
        case 17: _t->on_gopYButton_clicked(); break;
        case 18: _t->on_deNghiMuaSachButton_clicked(); break;
        case 19: _t->on_searchNotiButton_clicked(); break;
        default: ;
        }
    }
}

const QMetaObject ReaderForm::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_ReaderForm.data,
      qt_meta_data_ReaderForm,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *ReaderForm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ReaderForm::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_ReaderForm.stringdata0))
        return static_cast<void*>(const_cast< ReaderForm*>(this));
    return QWidget::qt_metacast(_clname);
}

int ReaderForm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 20)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 20;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 20)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 20;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
