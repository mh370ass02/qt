/****************************************************************************
** Meta object code from reading C++ file 'returnbookform.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../returnbookform.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'returnbookform.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ReturnBookForm_t {
    QByteArrayData data[15];
    char stringdata0[320];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ReturnBookForm_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ReturnBookForm_t qt_meta_stringdata_ReturnBookForm = {
    {
QT_MOC_LITERAL(0, 0, 14), // "ReturnBookForm"
QT_MOC_LITERAL(1, 15, 21), // "on_exitButton_clicked"
QT_MOC_LITERAL(2, 37, 0), // ""
QT_MOC_LITERAL(3, 38, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(4, 60, 25), // "on_quaHanCheckBox_toggled"
QT_MOC_LITERAL(5, 86, 7), // "checked"
QT_MOC_LITERAL(6, 94, 25), // "on_huHongCheckBox_toggled"
QT_MOC_LITERAL(7, 120, 26), // "on_matSachCheckBox_toggled"
QT_MOC_LITERAL(8, 147, 26), // "on_quaHanSpin_valueChanged"
QT_MOC_LITERAL(9, 174, 4), // "arg1"
QT_MOC_LITERAL(10, 179, 26), // "on_huHongSpin_valueChanged"
QT_MOC_LITERAL(11, 206, 27), // "on_matSachSpin_valueChanged"
QT_MOC_LITERAL(12, 234, 27), // "on_quaHanSpin2_valueChanged"
QT_MOC_LITERAL(13, 262, 27), // "on_huHongSpin2_valueChanged"
QT_MOC_LITERAL(14, 290, 29) // "on_matSachSpin_2_valueChanged"

    },
    "ReturnBookForm\0on_exitButton_clicked\0"
    "\0on_pushButton_clicked\0on_quaHanCheckBox_toggled\0"
    "checked\0on_huHongCheckBox_toggled\0"
    "on_matSachCheckBox_toggled\0"
    "on_quaHanSpin_valueChanged\0arg1\0"
    "on_huHongSpin_valueChanged\0"
    "on_matSachSpin_valueChanged\0"
    "on_quaHanSpin2_valueChanged\0"
    "on_huHongSpin2_valueChanged\0"
    "on_matSachSpin_2_valueChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ReturnBookForm[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   69,    2, 0x08 /* Private */,
       3,    0,   70,    2, 0x08 /* Private */,
       4,    1,   71,    2, 0x08 /* Private */,
       6,    1,   74,    2, 0x08 /* Private */,
       7,    1,   77,    2, 0x08 /* Private */,
       8,    1,   80,    2, 0x08 /* Private */,
      10,    1,   83,    2, 0x08 /* Private */,
      11,    1,   86,    2, 0x08 /* Private */,
      12,    1,   89,    2, 0x08 /* Private */,
      13,    1,   92,    2, 0x08 /* Private */,
      14,    1,   95,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    5,
    QMetaType::Void, QMetaType::Bool,    5,
    QMetaType::Void, QMetaType::Bool,    5,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void, QMetaType::Int,    9,

       0        // eod
};

void ReturnBookForm::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ReturnBookForm *_t = static_cast<ReturnBookForm *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_exitButton_clicked(); break;
        case 1: _t->on_pushButton_clicked(); break;
        case 2: _t->on_quaHanCheckBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->on_huHongCheckBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->on_matSachCheckBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->on_quaHanSpin_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->on_huHongSpin_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->on_matSachSpin_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->on_quaHanSpin2_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->on_huHongSpin2_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->on_matSachSpin_2_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject ReturnBookForm::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_ReturnBookForm.data,
      qt_meta_data_ReturnBookForm,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *ReturnBookForm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ReturnBookForm::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_ReturnBookForm.stringdata0))
        return static_cast<void*>(const_cast< ReturnBookForm*>(this));
    return QWidget::qt_metacast(_clname);
}

int ReturnBookForm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
