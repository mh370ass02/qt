/********************************************************************************
** Form generated from reading UI file 'librarianform.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LIBRARIANFORM_H
#define UI_LIBRARIANFORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LibrarianForm
{
public:
    QVBoxLayout *verticalLayout_8;
    QVBoxLayout *verticalLayout_7;
    QGridLayout *gridLayout_3;
    QSpacerItem *horizontalSpacer_14;
    QPushButton *user;
    QSpacerItem *horizontalSpacer_15;
    QLabel *label_3;
    QPushButton *logoutButton;
    QSpacerItem *horizontalSpacer_16;
    QSpacerItem *horizontalSpacer_17;
    QFrame *line;
    QComboBox *roleComboBox;
    QLabel *label_10;
    QSpacerItem *horizontalSpacer_18;
    QSpacerItem *horizontalSpacer_19;
    QTabWidget *tabWidget;
    QWidget *tab;
    QVBoxLayout *verticalLayout_5;
    QVBoxLayout *verticalLayout_3;
    QWidget *hoa;
    QGridLayout *gridLayout;
    QSpacerItem *verticalSpacer_4;
    QPushButton *searchButton;
    QSpacerItem *verticalSpacer_3;
    QTableView *bookTableView;
    QLabel *label_7;
    QLineEdit *searchBox;
    QComboBox *searchComboBox;
    QLabel *label;
    QComboBox *sortModeComboBox;
    QLabel *label_4;
    QPushButton *viewAllButton;
    QSpacerItem *verticalSpacer_7;
    QSpacerItem *verticalSpacer_2;
    QComboBox *sortComboBox;
    QLabel *label_9;
    QSpacerItem *verticalSpacer;
    QComboBox *typeComboBox;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QPushButton *addBookButton;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *viewMoreButton;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *deleteBookButton;
    QSpacerItem *horizontalSpacer_4;
    QSpacerItem *horizontalSpacer_5;
    QWidget *tab_2;
    QVBoxLayout *verticalLayout_6;
    QVBoxLayout *verticalLayout_4;
    QGridLayout *gridLayout_2;
    QSpacerItem *verticalSpacer_6;
    QSpacerItem *verticalSpacer_5;
    QPushButton *searchRequestButton;
    QLabel *label_8;
    QComboBox *typeRequestComboBox;
    QLineEdit *searchRequestBox;
    QLabel *label_2;
    QComboBox *searchRequestComboBox;
    QPushButton *viewAllRequestButton;
    QSpacerItem *verticalSpacer_8;
    QTableView *requestTableView;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *button;
    QSpacerItem *horizontalSpacer_21;
    QPushButton *extendButton;
    QSpacerItem *horizontalSpacer_7;
    QPushButton *deleteButton;
    QSpacerItem *horizontalSpacer_8;
    QSpacerItem *horizontalSpacer_9;
    QWidget *tab_3;
    QVBoxLayout *verticalLayout_11;
    QVBoxLayout *verticalLayout_10;
    QGridLayout *gridLayout_4;
    QHBoxLayout *horizontalLayout_4;
    QLineEdit *searchNotiBox;
    QPushButton *searchNotiButton;
    QTableView *notiTableView;
    QVBoxLayout *verticalLayout_9;
    QSpacerItem *verticalSpacer_10;
    QLabel *label_14;
    QComboBox *typeNotiComboBox;
    QSpacerItem *verticalSpacer_9;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_10;
    QPushButton *editNotiButton;
    QSpacerItem *horizontalSpacer_11;
    QPushButton *sendNotiButton;
    QSpacerItem *horizontalSpacer_12;
    QPushButton *deleteNotiButton;
    QSpacerItem *horizontalSpacer_13;
    QSpacerItem *horizontalSpacer_20;
    QWidget *tab_4;
    QPushButton *updateSettingButton;
    QWidget *layoutWidget;
    QGridLayout *gridLayout_5;
    QLabel *label_5;
    QSpinBox *spinBox_0;
    QLabel *label_6;
    QSpinBox *spinBox_1;
    QLabel *label_11;
    QSpinBox *spinBox_2;
    QLabel *label_12;
    QSpinBox *spinBox_3;
    QLabel *label_13;
    QDoubleSpinBox *spinBox_4;
    QLabel *label_15;
    QSpinBox *spinBox_5;
    QLabel *label_16;
    QSpinBox *spinBox_6;

    void setupUi(QWidget *LibrarianForm)
    {
        if (LibrarianForm->objectName().isEmpty())
            LibrarianForm->setObjectName(QStringLiteral("LibrarianForm"));
        LibrarianForm->resize(1360, 730);
        LibrarianForm->setMinimumSize(QSize(1360, 630));
        LibrarianForm->setMaximumSize(QSize(16777215, 16777215));
        QIcon icon;
        icon.addFile(QStringLiteral(":/images/setting.png"), QSize(), QIcon::Normal, QIcon::Off);
        LibrarianForm->setWindowIcon(icon);
        verticalLayout_8 = new QVBoxLayout(LibrarianForm);
        verticalLayout_8->setSpacing(6);
        verticalLayout_8->setContentsMargins(11, 11, 11, 11);
        verticalLayout_8->setObjectName(QStringLiteral("verticalLayout_8"));
        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setSpacing(6);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        horizontalSpacer_14 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer_14, 0, 1, 1, 1);

        user = new QPushButton(LibrarianForm);
        user->setObjectName(QStringLiteral("user"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(user->sizePolicy().hasHeightForWidth());
        user->setSizePolicy(sizePolicy);
        QFont font;
        font.setPointSize(11);
        user->setFont(font);
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/images/user.png"), QSize(), QIcon::Normal, QIcon::Off);
        user->setIcon(icon1);
        user->setIconSize(QSize(24, 24));

        gridLayout_3->addWidget(user, 0, 3, 1, 1);

        horizontalSpacer_15 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer_15, 2, 1, 1, 1);

        label_3 = new QLabel(LibrarianForm);
        label_3->setObjectName(QStringLiteral("label_3"));
        QFont font1;
        font1.setPointSize(18);
        font1.setBold(true);
        font1.setWeight(75);
        label_3->setFont(font1);

        gridLayout_3->addWidget(label_3, 0, 2, 1, 1);

        logoutButton = new QPushButton(LibrarianForm);
        logoutButton->setObjectName(QStringLiteral("logoutButton"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(logoutButton->sizePolicy().hasHeightForWidth());
        logoutButton->setSizePolicy(sizePolicy1);
        logoutButton->setMinimumSize(QSize(180, 0));
        logoutButton->setFont(font);
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/images/signout.png"), QSize(), QIcon::Normal, QIcon::Off);
        logoutButton->setIcon(icon2);
        logoutButton->setIconSize(QSize(24, 24));

        gridLayout_3->addWidget(logoutButton, 0, 4, 1, 1);

        horizontalSpacer_16 = new QSpacerItem(168, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer_16, 2, 0, 2, 1);

        horizontalSpacer_17 = new QSpacerItem(108, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer_17, 2, 2, 2, 1);

        line = new QFrame(LibrarianForm);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout_3->addWidget(line, 1, 0, 1, 5);

        roleComboBox = new QComboBox(LibrarianForm);
        roleComboBox->setObjectName(QStringLiteral("roleComboBox"));
        sizePolicy1.setHeightForWidth(roleComboBox->sizePolicy().hasHeightForWidth());
        roleComboBox->setSizePolicy(sizePolicy1);
        roleComboBox->setMinimumSize(QSize(180, 0));
        roleComboBox->setFont(font);
        roleComboBox->setStyleSheet(QLatin1String("#roleComboBox{\n"
"text-align : center;\n"
"}"));

        gridLayout_3->addWidget(roleComboBox, 2, 4, 2, 1);

        label_10 = new QLabel(LibrarianForm);
        label_10->setObjectName(QStringLiteral("label_10"));

        gridLayout_3->addWidget(label_10, 3, 3, 1, 1);

        horizontalSpacer_18 = new QSpacerItem(168, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer_18, 0, 0, 1, 1);

        horizontalSpacer_19 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer_19, 2, 3, 1, 1);


        verticalLayout_7->addLayout(gridLayout_3);

        tabWidget = new QTabWidget(LibrarianForm);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        QFont font2;
        font2.setPointSize(11);
        font2.setBold(true);
        font2.setWeight(75);
        tabWidget->setFont(font2);
        tabWidget->setTabShape(QTabWidget::Rounded);
        tabWidget->setIconSize(QSize(32, 32));
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        verticalLayout_5 = new QVBoxLayout(tab);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        hoa = new QWidget(tab);
        hoa->setObjectName(QStringLiteral("hoa"));
        QFont font3;
        font3.setBold(false);
        font3.setWeight(50);
        hoa->setFont(font3);
        hoa->setStyleSheet(QStringLiteral(""));
        gridLayout = new QGridLayout(hoa);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_4, 5, 1, 1, 1);

        searchButton = new QPushButton(hoa);
        searchButton->setObjectName(QStringLiteral("searchButton"));
        QFont font4;
        font4.setPointSize(11);
        font4.setBold(false);
        font4.setWeight(50);
        searchButton->setFont(font4);
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/images/search.png"), QSize(), QIcon::Normal, QIcon::Off);
        searchButton->setIcon(icon3);
        searchButton->setIconSize(QSize(24, 24));

        gridLayout->addWidget(searchButton, 0, 1, 1, 3);

        verticalSpacer_3 = new QSpacerItem(77, 37, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_3, 1, 1, 1, 1);

        bookTableView = new QTableView(hoa);
        bookTableView->setObjectName(QStringLiteral("bookTableView"));
        bookTableView->setToolTipDuration(-1);
        bookTableView->setEditTriggers(QAbstractItemView::AllEditTriggers);

        gridLayout->addWidget(bookTableView, 1, 0, 15, 1);

        label_7 = new QLabel(hoa);
        label_7->setObjectName(QStringLiteral("label_7"));
        QFont font5;
        font5.setPointSize(9);
        font5.setBold(false);
        font5.setWeight(50);
        label_7->setFont(font5);

        gridLayout->addWidget(label_7, 2, 1, 1, 1);

        searchBox = new QLineEdit(hoa);
        searchBox->setObjectName(QStringLiteral("searchBox"));
        searchBox->setFont(font);

        gridLayout->addWidget(searchBox, 0, 0, 1, 1);

        searchComboBox = new QComboBox(hoa);
        searchComboBox->setObjectName(QStringLiteral("searchComboBox"));
        QFont font6;
        font6.setPointSize(10);
        font6.setBold(false);
        font6.setWeight(50);
        searchComboBox->setFont(font6);

        gridLayout->addWidget(searchComboBox, 7, 1, 1, 3);

        label = new QLabel(hoa);
        label->setObjectName(QStringLiteral("label"));
        label->setFont(font5);

        gridLayout->addWidget(label, 6, 1, 1, 3);

        sortModeComboBox = new QComboBox(hoa);
        sortModeComboBox->setObjectName(QStringLiteral("sortModeComboBox"));
        sortModeComboBox->setFont(font6);

        gridLayout->addWidget(sortModeComboBox, 13, 1, 1, 3);

        label_4 = new QLabel(hoa);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setFont(font5);

        gridLayout->addWidget(label_4, 9, 1, 1, 1);

        viewAllButton = new QPushButton(hoa);
        viewAllButton->setObjectName(QStringLiteral("viewAllButton"));
        viewAllButton->setFont(font);

        gridLayout->addWidget(viewAllButton, 15, 1, 1, 3);

        verticalSpacer_7 = new QSpacerItem(77, 37, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_7, 11, 1, 1, 1);

        verticalSpacer_2 = new QSpacerItem(77, 37, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_2, 8, 1, 1, 1);

        sortComboBox = new QComboBox(hoa);
        sortComboBox->setObjectName(QStringLiteral("sortComboBox"));
        sortComboBox->setFont(font6);

        gridLayout->addWidget(sortComboBox, 10, 1, 1, 3);

        label_9 = new QLabel(hoa);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setFont(font5);

        gridLayout->addWidget(label_9, 12, 1, 1, 1);

        verticalSpacer = new QSpacerItem(121, 37, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 14, 1, 1, 2);

        typeComboBox = new QComboBox(hoa);
        typeComboBox->setObjectName(QStringLiteral("typeComboBox"));
        typeComboBox->setFont(font6);

        gridLayout->addWidget(typeComboBox, 3, 1, 1, 3);


        verticalLayout_3->addWidget(hoa);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        addBookButton = new QPushButton(tab);
        addBookButton->setObjectName(QStringLiteral("addBookButton"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Maximum);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(addBookButton->sizePolicy().hasHeightForWidth());
        addBookButton->setSizePolicy(sizePolicy2);
        addBookButton->setMaximumSize(QSize(16777215, 42));
        addBookButton->setFont(font2);
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/images/addBook.png"), QSize(), QIcon::Normal, QIcon::Off);
        addBookButton->setIcon(icon4);
        addBookButton->setIconSize(QSize(48, 48));

        horizontalLayout_2->addWidget(addBookButton);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        viewMoreButton = new QPushButton(tab);
        viewMoreButton->setObjectName(QStringLiteral("viewMoreButton"));
        sizePolicy2.setHeightForWidth(viewMoreButton->sizePolicy().hasHeightForWidth());
        viewMoreButton->setSizePolicy(sizePolicy2);
        viewMoreButton->setMaximumSize(QSize(16777215, 42));
        viewMoreButton->setFont(font2);
        QIcon icon5;
        icon5.addFile(QStringLiteral(":/images/editBook.png"), QSize(), QIcon::Normal, QIcon::Off);
        viewMoreButton->setIcon(icon5);
        viewMoreButton->setIconSize(QSize(48, 48));

        horizontalLayout_2->addWidget(viewMoreButton);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);

        deleteBookButton = new QPushButton(tab);
        deleteBookButton->setObjectName(QStringLiteral("deleteBookButton"));
        sizePolicy2.setHeightForWidth(deleteBookButton->sizePolicy().hasHeightForWidth());
        deleteBookButton->setSizePolicy(sizePolicy2);
        deleteBookButton->setMaximumSize(QSize(16777215, 42));
        deleteBookButton->setFont(font2);
        QIcon icon6;
        icon6.addFile(QStringLiteral(":/images/deleteBook.png"), QSize(), QIcon::Normal, QIcon::Off);
        deleteBookButton->setIcon(icon6);
        deleteBookButton->setIconSize(QSize(48, 48));

        horizontalLayout_2->addWidget(deleteBookButton);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_4);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_5);


        verticalLayout_3->addLayout(horizontalLayout_2);


        verticalLayout_5->addLayout(verticalLayout_3);

        QIcon icon7;
        icon7.addFile(QStringLiteral(":/images/book.png"), QSize(), QIcon::Normal, QIcon::Off);
        tabWidget->addTab(tab, icon7, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        verticalLayout_6 = new QVBoxLayout(tab_2);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        verticalSpacer_6 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer_6, 1, 1, 1, 1);

        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer_5, 7, 1, 1, 1);

        searchRequestButton = new QPushButton(tab_2);
        searchRequestButton->setObjectName(QStringLiteral("searchRequestButton"));
        searchRequestButton->setMinimumSize(QSize(0, 30));
        QFont font7;
        font7.setPointSize(12);
        font7.setBold(false);
        font7.setWeight(50);
        searchRequestButton->setFont(font7);
        searchRequestButton->setIcon(icon3);

        gridLayout_2->addWidget(searchRequestButton, 0, 1, 1, 1);

        label_8 = new QLabel(tab_2);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setFont(font5);

        gridLayout_2->addWidget(label_8, 5, 1, 1, 1);

        typeRequestComboBox = new QComboBox(tab_2);
        typeRequestComboBox->setObjectName(QStringLiteral("typeRequestComboBox"));
        typeRequestComboBox->setFont(font6);

        gridLayout_2->addWidget(typeRequestComboBox, 6, 1, 1, 1);

        searchRequestBox = new QLineEdit(tab_2);
        searchRequestBox->setObjectName(QStringLiteral("searchRequestBox"));

        gridLayout_2->addWidget(searchRequestBox, 0, 0, 1, 1);

        label_2 = new QLabel(tab_2);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setFont(font5);

        gridLayout_2->addWidget(label_2, 2, 1, 1, 1);

        searchRequestComboBox = new QComboBox(tab_2);
        searchRequestComboBox->setObjectName(QStringLiteral("searchRequestComboBox"));
        searchRequestComboBox->setFont(font6);

        gridLayout_2->addWidget(searchRequestComboBox, 3, 1, 1, 1);

        viewAllRequestButton = new QPushButton(tab_2);
        viewAllRequestButton->setObjectName(QStringLiteral("viewAllRequestButton"));
        viewAllRequestButton->setMinimumSize(QSize(0, 32));
        viewAllRequestButton->setFont(font6);

        gridLayout_2->addWidget(viewAllRequestButton, 8, 1, 1, 1);

        verticalSpacer_8 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer_8, 4, 1, 1, 1);

        requestTableView = new QTableView(tab_2);
        requestTableView->setObjectName(QStringLiteral("requestTableView"));

        gridLayout_2->addWidget(requestTableView, 1, 0, 8, 1);


        verticalLayout_4->addLayout(gridLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_6);

        button = new QPushButton(tab_2);
        button->setObjectName(QStringLiteral("button"));
        QSizePolicy sizePolicy3(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(button->sizePolicy().hasHeightForWidth());
        button->setSizePolicy(sizePolicy3);
        button->setMinimumSize(QSize(300, 45));
        button->setMaximumSize(QSize(16777215, 45));
        button->setFont(font2);
        QIcon icon8;
        icon8.addFile(QStringLiteral(":/images/returnBook.png"), QSize(), QIcon::Normal, QIcon::Off);
        button->setIcon(icon8);
        button->setIconSize(QSize(32, 32));

        horizontalLayout_3->addWidget(button);

        horizontalSpacer_21 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_21);

        extendButton = new QPushButton(tab_2);
        extendButton->setObjectName(QStringLiteral("extendButton"));
        sizePolicy3.setHeightForWidth(extendButton->sizePolicy().hasHeightForWidth());
        extendButton->setSizePolicy(sizePolicy3);
        extendButton->setMinimumSize(QSize(0, 45));
        QIcon icon9;
        icon9.addFile(QStringLiteral(":/images/extern.png"), QSize(), QIcon::Normal, QIcon::Off);
        extendButton->setIcon(icon9);
        extendButton->setIconSize(QSize(32, 32));

        horizontalLayout_3->addWidget(extendButton);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_7);

        deleteButton = new QPushButton(tab_2);
        deleteButton->setObjectName(QStringLiteral("deleteButton"));
        sizePolicy3.setHeightForWidth(deleteButton->sizePolicy().hasHeightForWidth());
        deleteButton->setSizePolicy(sizePolicy3);
        deleteButton->setMinimumSize(QSize(0, 45));
        deleteButton->setMaximumSize(QSize(16777215, 45));
        deleteButton->setFont(font2);
        QIcon icon10;
        icon10.addFile(QStringLiteral(":/images/deleteRecord.png"), QSize(), QIcon::Normal, QIcon::Off);
        deleteButton->setIcon(icon10);
        deleteButton->setIconSize(QSize(36, 36));

        horizontalLayout_3->addWidget(deleteButton);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_8);

        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_9);


        verticalLayout_4->addLayout(horizontalLayout_3);


        verticalLayout_6->addLayout(verticalLayout_4);

        QIcon icon11;
        icon11.addFile(QStringLiteral(":/images/bookBorrow.png"), QSize(), QIcon::Normal, QIcon::Off);
        tabWidget->addTab(tab_2, icon11, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QStringLiteral("tab_3"));
        verticalLayout_11 = new QVBoxLayout(tab_3);
        verticalLayout_11->setSpacing(6);
        verticalLayout_11->setContentsMargins(11, 11, 11, 11);
        verticalLayout_11->setObjectName(QStringLiteral("verticalLayout_11"));
        verticalLayout_10 = new QVBoxLayout();
        verticalLayout_10->setSpacing(6);
        verticalLayout_10->setObjectName(QStringLiteral("verticalLayout_10"));
        gridLayout_4 = new QGridLayout();
        gridLayout_4->setSpacing(6);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        searchNotiBox = new QLineEdit(tab_3);
        searchNotiBox->setObjectName(QStringLiteral("searchNotiBox"));
        sizePolicy3.setHeightForWidth(searchNotiBox->sizePolicy().hasHeightForWidth());
        searchNotiBox->setSizePolicy(sizePolicy3);
        searchNotiBox->setMinimumSize(QSize(0, 30));
        searchNotiBox->setMaximumSize(QSize(16777215, 30));
        searchNotiBox->setFont(font);

        horizontalLayout_4->addWidget(searchNotiBox);

        searchNotiButton = new QPushButton(tab_3);
        searchNotiButton->setObjectName(QStringLiteral("searchNotiButton"));
        sizePolicy1.setHeightForWidth(searchNotiButton->sizePolicy().hasHeightForWidth());
        searchNotiButton->setSizePolicy(sizePolicy1);
        searchNotiButton->setMinimumSize(QSize(180, 30));
        searchNotiButton->setMaximumSize(QSize(180, 30));
        searchNotiButton->setFont(font);
        searchNotiButton->setIcon(icon3);
        searchNotiButton->setIconSize(QSize(24, 24));

        horizontalLayout_4->addWidget(searchNotiButton);


        gridLayout_4->addLayout(horizontalLayout_4, 0, 0, 1, 2);

        notiTableView = new QTableView(tab_3);
        notiTableView->setObjectName(QStringLiteral("notiTableView"));

        gridLayout_4->addWidget(notiTableView, 1, 0, 1, 1);

        verticalLayout_9 = new QVBoxLayout();
        verticalLayout_9->setSpacing(6);
        verticalLayout_9->setObjectName(QStringLiteral("verticalLayout_9"));
        verticalSpacer_10 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_9->addItem(verticalSpacer_10);

        label_14 = new QLabel(tab_3);
        label_14->setObjectName(QStringLiteral("label_14"));
        QSizePolicy sizePolicy4(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(label_14->sizePolicy().hasHeightForWidth());
        label_14->setSizePolicy(sizePolicy4);
        label_14->setMinimumSize(QSize(180, 0));
        label_14->setMaximumSize(QSize(180, 16777215));
        label_14->setFont(font5);

        verticalLayout_9->addWidget(label_14);

        typeNotiComboBox = new QComboBox(tab_3);
        typeNotiComboBox->setObjectName(QStringLiteral("typeNotiComboBox"));
        sizePolicy1.setHeightForWidth(typeNotiComboBox->sizePolicy().hasHeightForWidth());
        typeNotiComboBox->setSizePolicy(sizePolicy1);
        typeNotiComboBox->setMinimumSize(QSize(180, 0));
        typeNotiComboBox->setMaximumSize(QSize(180, 16777215));
        typeNotiComboBox->setFont(font4);

        verticalLayout_9->addWidget(typeNotiComboBox);

        verticalSpacer_9 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_9->addItem(verticalSpacer_9);


        gridLayout_4->addLayout(verticalLayout_9, 1, 1, 1, 1);


        verticalLayout_10->addLayout(gridLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_10);

        editNotiButton = new QPushButton(tab_3);
        editNotiButton->setObjectName(QStringLiteral("editNotiButton"));
        sizePolicy3.setHeightForWidth(editNotiButton->sizePolicy().hasHeightForWidth());
        editNotiButton->setSizePolicy(sizePolicy3);
        editNotiButton->setMinimumSize(QSize(300, 0));
        editNotiButton->setFont(font2);
        QIcon icon12;
        icon12.addFile(QStringLiteral(":/images/mail check.png"), QSize(), QIcon::Normal, QIcon::Off);
        editNotiButton->setIcon(icon12);
        editNotiButton->setIconSize(QSize(38, 38));

        horizontalLayout_5->addWidget(editNotiButton);

        horizontalSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_11);

        sendNotiButton = new QPushButton(tab_3);
        sendNotiButton->setObjectName(QStringLiteral("sendNotiButton"));
        sizePolicy3.setHeightForWidth(sendNotiButton->sizePolicy().hasHeightForWidth());
        sendNotiButton->setSizePolicy(sizePolicy3);
        sendNotiButton->setMinimumSize(QSize(200, 0));
        sendNotiButton->setFont(font2);
        QIcon icon13;
        icon13.addFile(QStringLiteral(":/images/mail send.png"), QSize(), QIcon::Normal, QIcon::Off);
        sendNotiButton->setIcon(icon13);
        sendNotiButton->setIconSize(QSize(38, 38));

        horizontalLayout_5->addWidget(sendNotiButton);

        horizontalSpacer_12 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_12);

        deleteNotiButton = new QPushButton(tab_3);
        deleteNotiButton->setObjectName(QStringLiteral("deleteNotiButton"));
        deleteNotiButton->setMinimumSize(QSize(200, 0));
        deleteNotiButton->setFont(font2);
        QIcon icon14;
        icon14.addFile(QStringLiteral(":/images/mail delete.png"), QSize(), QIcon::Normal, QIcon::Off);
        deleteNotiButton->setIcon(icon14);
        deleteNotiButton->setIconSize(QSize(38, 38));

        horizontalLayout_5->addWidget(deleteNotiButton);

        horizontalSpacer_13 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_13);

        horizontalSpacer_20 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_20);


        verticalLayout_10->addLayout(horizontalLayout_5);


        verticalLayout_11->addLayout(verticalLayout_10);

        QIcon icon15;
        icon15.addFile(QStringLiteral(":/images/mail.png"), QSize(), QIcon::Normal, QIcon::Off);
        tabWidget->addTab(tab_3, icon15, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QStringLiteral("tab_4"));
        updateSettingButton = new QPushButton(tab_4);
        updateSettingButton->setObjectName(QStringLiteral("updateSettingButton"));
        updateSettingButton->setGeometry(QRect(490, 510, 211, 51));
        QIcon icon16;
        icon16.addFile(QStringLiteral(":/images/settingCheck.png"), QSize(), QIcon::Normal, QIcon::Off);
        updateSettingButton->setIcon(icon16);
        updateSettingButton->setIconSize(QSize(48, 48));
        layoutWidget = new QWidget(tab_4);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(360, 140, 604, 206));
        gridLayout_5 = new QGridLayout(layoutWidget);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        gridLayout_5->setContentsMargins(0, 0, 0, 0);
        label_5 = new QLabel(layoutWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setFont(font3);

        gridLayout_5->addWidget(label_5, 0, 0, 1, 1);

        spinBox_0 = new QSpinBox(layoutWidget);
        spinBox_0->setObjectName(QStringLiteral("spinBox_0"));
        spinBox_0->setInputMethodHints(Qt::ImhDigitsOnly);
        spinBox_0->setMaximum(1000);

        gridLayout_5->addWidget(spinBox_0, 0, 1, 1, 1);

        label_6 = new QLabel(layoutWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        sizePolicy4.setHeightForWidth(label_6->sizePolicy().hasHeightForWidth());
        label_6->setSizePolicy(sizePolicy4);
        label_6->setMinimumSize(QSize(500, 0));
        label_6->setFont(font3);

        gridLayout_5->addWidget(label_6, 1, 0, 1, 1);

        spinBox_1 = new QSpinBox(layoutWidget);
        spinBox_1->setObjectName(QStringLiteral("spinBox_1"));
        spinBox_1->setInputMethodHints(Qt::ImhDigitsOnly);
        spinBox_1->setMaximum(1000);

        gridLayout_5->addWidget(spinBox_1, 1, 1, 1, 1);

        label_11 = new QLabel(layoutWidget);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setFont(font3);

        gridLayout_5->addWidget(label_11, 2, 0, 1, 1);

        spinBox_2 = new QSpinBox(layoutWidget);
        spinBox_2->setObjectName(QStringLiteral("spinBox_2"));
        spinBox_2->setInputMethodHints(Qt::ImhDigitsOnly);
        spinBox_2->setMaximum(1000000);
        spinBox_2->setSingleStep(500);

        gridLayout_5->addWidget(spinBox_2, 2, 1, 1, 1);

        label_12 = new QLabel(layoutWidget);
        label_12->setObjectName(QStringLiteral("label_12"));
        sizePolicy4.setHeightForWidth(label_12->sizePolicy().hasHeightForWidth());
        label_12->setSizePolicy(sizePolicy4);
        label_12->setMinimumSize(QSize(400, 0));
        label_12->setFont(font3);

        gridLayout_5->addWidget(label_12, 3, 0, 1, 1);

        spinBox_3 = new QSpinBox(layoutWidget);
        spinBox_3->setObjectName(QStringLiteral("spinBox_3"));
        spinBox_3->setInputMethodHints(Qt::ImhDigitsOnly);
        spinBox_3->setMaximum(1000);

        gridLayout_5->addWidget(spinBox_3, 3, 1, 1, 1);

        label_13 = new QLabel(layoutWidget);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setFont(font3);

        gridLayout_5->addWidget(label_13, 4, 0, 1, 1);

        spinBox_4 = new QDoubleSpinBox(layoutWidget);
        spinBox_4->setObjectName(QStringLiteral("spinBox_4"));
        spinBox_4->setSingleStep(0.05);

        gridLayout_5->addWidget(spinBox_4, 4, 1, 1, 1);

        label_15 = new QLabel(layoutWidget);
        label_15->setObjectName(QStringLiteral("label_15"));
        label_15->setFont(font3);

        gridLayout_5->addWidget(label_15, 5, 0, 1, 1);

        spinBox_5 = new QSpinBox(layoutWidget);
        spinBox_5->setObjectName(QStringLiteral("spinBox_5"));
        spinBox_5->setInputMethodHints(Qt::ImhDigitsOnly);
        spinBox_5->setMaximum(1000);

        gridLayout_5->addWidget(spinBox_5, 5, 1, 1, 1);

        label_16 = new QLabel(layoutWidget);
        label_16->setObjectName(QStringLiteral("label_16"));
        label_16->setFont(font3);

        gridLayout_5->addWidget(label_16, 6, 0, 1, 1);

        spinBox_6 = new QSpinBox(layoutWidget);
        spinBox_6->setObjectName(QStringLiteral("spinBox_6"));
        spinBox_6->setInputMethodHints(Qt::ImhDigitsOnly);
        spinBox_6->setMaximum(1000);

        gridLayout_5->addWidget(spinBox_6, 6, 1, 1, 1);

        tabWidget->addTab(tab_4, icon, QString());

        verticalLayout_7->addWidget(tabWidget);


        verticalLayout_8->addLayout(verticalLayout_7);


        retranslateUi(LibrarianForm);

        tabWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(LibrarianForm);
    } // setupUi

    void retranslateUi(QWidget *LibrarianForm)
    {
        LibrarianForm->setWindowTitle(QApplication::translate("LibrarianForm", "LibPro: Th\341\273\247 Th\306\260", Q_NULLPTR));
        user->setText(QApplication::translate("LibrarianForm", "T\303\240i Kho\341\272\243n", Q_NULLPTR));
        label_3->setText(QApplication::translate("LibrarianForm", "TH\341\273\246 TH\306\257", Q_NULLPTR));
        logoutButton->setText(QApplication::translate("LibrarianForm", "\304\220\304\203ng Xu\341\272\245t", Q_NULLPTR));
        label_10->setText(QString());
        searchButton->setText(QApplication::translate("LibrarianForm", "T\303\254m ki\341\272\277m", Q_NULLPTR));
        label_7->setText(QApplication::translate("LibrarianForm", "Danh m\341\273\245c", Q_NULLPTR));
        searchBox->setText(QString());
        searchComboBox->clear();
        searchComboBox->insertItems(0, QStringList()
         << QApplication::translate("LibrarianForm", "T\341\272\245t c\341\272\243", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "M\303\243 s\303\241ch", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "T\303\252n s\303\241ch", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "Ch\341\273\247 \304\221\341\273\201", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "T\303\252n t\303\241c gi\341\272\243", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "Nh\303\240 xu\341\272\245t b\341\272\243n", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "N\304\203m xu\341\272\245t b\341\272\243n", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "S\341\273\221 s\303\241ch \304\221\303\243 m\306\260\341\273\243n", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "T\303\263m t\341\272\257t n\341\273\231i dung", Q_NULLPTR)
        );
        label->setText(QApplication::translate("LibrarianForm", "T\303\254m ki\341\272\277m theo", Q_NULLPTR));
        sortModeComboBox->clear();
        sortModeComboBox->insertItems(0, QStringList()
         << QApplication::translate("LibrarianForm", "T\304\203ng d\341\272\247n", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "Gi\341\272\243m d\341\272\247n", Q_NULLPTR)
        );
        label_4->setText(QApplication::translate("LibrarianForm", "S\341\272\257p x\341\272\277p theo:", Q_NULLPTR));
        viewAllButton->setText(QApplication::translate("LibrarianForm", "Xem t\341\272\245t c\341\272\243", Q_NULLPTR));
        sortComboBox->clear();
        sortComboBox->insertItems(0, QStringList()
         << QApplication::translate("LibrarianForm", "M\341\273\233i v\341\273\201", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "M\303\243 s\303\241ch", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "T\303\252n s\303\241ch", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "Lo\341\272\241i s\303\241ch", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "T\303\252n t\303\241c gi\341\272\243", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "Nh\303\240 xu\341\272\245t b\341\272\243n", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "N\304\203m xu\341\272\245t b\341\272\243n", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "S\341\273\221 s\303\241ch \304\221\303\243 m\306\260\341\273\243n", Q_NULLPTR)
        );
        label_9->setText(QApplication::translate("LibrarianForm", "Th\341\273\251 t\341\273\261:", Q_NULLPTR));
        typeComboBox->clear();
        typeComboBox->insertItems(0, QStringList()
         << QApplication::translate("LibrarianForm", "T\341\272\245t c\341\272\243", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "S\303\241ch", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "T\341\272\241p ch\303\255", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "Lu\341\272\255n \303\241n ti\341\272\277n s\304\251", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "Lu\341\272\255n v\304\203n th\341\272\241c s\304\251", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "Ti\303\252u chu\341\272\251n k\341\273\271 thu\341\272\255t", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "B\303\241o c\303\241o khoa h\341\273\215c", Q_NULLPTR)
        );
        addBookButton->setText(QApplication::translate("LibrarianForm", "Th\303\252m s\303\241ch", Q_NULLPTR));
        viewMoreButton->setText(QApplication::translate("LibrarianForm", "Xem/Ch\341\273\211nh s\341\273\255a", Q_NULLPTR));
        deleteBookButton->setText(QApplication::translate("LibrarianForm", "X\303\263a", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("LibrarianForm", "S\303\241ch", Q_NULLPTR));
        searchRequestButton->setText(QApplication::translate("LibrarianForm", "T\303\254m ki\341\272\277m", Q_NULLPTR));
        label_8->setText(QApplication::translate("LibrarianForm", "Danh m\341\273\245c", Q_NULLPTR));
        typeRequestComboBox->clear();
        typeRequestComboBox->insertItems(0, QStringList()
         << QApplication::translate("LibrarianForm", "T\341\272\245t c\341\272\243", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "Gi\341\273\217 s\303\241ch", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "Ch\341\273\235 m\306\260\341\273\243n", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "\304\220ang m\306\260\341\273\243n", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "Qu\303\241 h\341\272\241n", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "\304\220\303\243 m\306\260\341\273\243n", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "\304\220\303\243 ph\341\272\241t", Q_NULLPTR)
        );
        searchRequestBox->setText(QString());
        label_2->setText(QApplication::translate("LibrarianForm", "T\303\254m ki\341\272\277m theo", Q_NULLPTR));
        searchRequestComboBox->clear();
        searchRequestComboBox->insertItems(0, QStringList()
         << QApplication::translate("LibrarianForm", "T\341\272\245t c\341\272\243", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "userName", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "H\341\273\215 T\303\252n", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "CMND", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "MSSV", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "M\303\243 s\303\241ch", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "T\303\252n s\303\241ch", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "Y\303\252u c\341\272\247u", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "M\306\260\341\273\243n", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "H\341\272\277t h\341\272\241n", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "Tr\341\272\243", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "Ch\303\272 th\303\255ch", Q_NULLPTR)
        );
        viewAllRequestButton->setText(QApplication::translate("LibrarianForm", "Xem t\341\272\245t c\341\272\243", Q_NULLPTR));
        button->setText(QApplication::translate("LibrarianForm", "Cho m\306\260\341\273\243n/Tr\341\272\243 s\303\241ch/Ph\341\272\241t", Q_NULLPTR));
        extendButton->setText(QApplication::translate("LibrarianForm", "Gia h\341\272\241n", Q_NULLPTR));
        deleteButton->setText(QApplication::translate("LibrarianForm", "H\341\273\247y y\303\252u c\341\272\247u", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("LibrarianForm", "Y\303\252u c\341\272\247u", Q_NULLPTR));
        searchNotiBox->setText(QString());
        searchNotiButton->setText(QApplication::translate("LibrarianForm", "T\303\254m ki\341\272\277m", Q_NULLPTR));
        label_14->setText(QApplication::translate("LibrarianForm", "Danh m\341\273\245c", Q_NULLPTR));
        typeNotiComboBox->clear();
        typeNotiComboBox->insertItems(0, QStringList()
         << QApplication::translate("LibrarianForm", "T\341\272\245t c\341\272\243", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "Ch\306\260a \304\221\341\273\215c", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "\304\220\303\243 \304\221\341\273\215c", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "\304\220\341\273\201 ngh\341\273\213 mua t\303\240i li\341\273\207u", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "Y\303\252u c\341\272\247u h\341\273\227 tr\341\273\243", Q_NULLPTR)
         << QApplication::translate("LibrarianForm", "G\303\263p \303\275", Q_NULLPTR)
        );
        editNotiButton->setText(QApplication::translate("LibrarianForm", "Xem/ Ch\341\273\211nh s\341\273\255a th\303\264ng b\303\241o", Q_NULLPTR));
        sendNotiButton->setText(QApplication::translate("LibrarianForm", "G\341\273\255i th\303\264ng b\303\241o", Q_NULLPTR));
        deleteNotiButton->setText(QApplication::translate("LibrarianForm", "X\303\263a th\303\264ng b\303\241o", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QApplication::translate("LibrarianForm", "Th\303\264ng b\303\241o", Q_NULLPTR));
        updateSettingButton->setText(QApplication::translate("LibrarianForm", "C\341\272\255p nh\341\272\255t", Q_NULLPTR));
        label_5->setText(QApplication::translate("LibrarianForm", "S\341\273\221 s\303\241ch t\341\273\221i \304\221a \304\221\306\260\341\273\243c m\306\260\341\273\243n \304\221\341\273\221i v\341\273\233i Sinh Vi\303\252n", Q_NULLPTR));
        label_6->setText(QApplication::translate("LibrarianForm", "S\341\273\221 s\303\241ch t\341\273\221i \304\221a \304\221\306\260\341\273\243c m\306\260\341\273\243n \304\221\341\273\221i v\341\273\233i Gi\341\272\243ng Vi\303\252n/C\303\264ng nh\303\242n vi\303\252n ch\341\273\251c", Q_NULLPTR));
        label_11->setText(QApplication::translate("LibrarianForm", "Ph\303\255 ph\341\272\241t tr\341\273\205 h\341\272\241n s\303\241ch 1 ng\303\240y", Q_NULLPTR));
        label_12->setText(QApplication::translate("LibrarianForm", "S\341\273\221 l\306\260\341\273\243ng quy\341\273\203n t\341\273\221i \304\221a 1 s\303\241ch (c\303\271ng t\303\252n) \304\221\306\260\341\273\243c m\306\260\341\273\243n", Q_NULLPTR));
        label_13->setText(QApplication::translate("LibrarianForm", "H\341\273\207 s\341\273\221 ph\341\272\241t m\341\272\245t s\303\241ch (ph\341\272\241t x l\341\272\247n gi\303\241 s\303\241ch)", Q_NULLPTR));
        label_15->setText(QApplication::translate("LibrarianForm", "S\341\273\221 ng\303\240y ch\341\273\235 m\306\260\341\273\243n t\341\273\221i \304\221a", Q_NULLPTR));
        label_16->setText(QApplication::translate("LibrarianForm", "S\341\273\221 ng\303\240y qu\303\241 h\341\272\241n t\341\273\221i \304\221a tr\306\260\341\273\233c khi b\341\273\213 kh\303\263a", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QApplication::translate("LibrarianForm", "C\303\240i \304\221\341\272\267t", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class LibrarianForm: public Ui_LibrarianForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LIBRARIANFORM_H
