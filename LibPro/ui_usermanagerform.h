/********************************************************************************
** Form generated from reading UI file 'usermanagerform.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_USERMANAGERFORM_H
#define UI_USERMANAGERFORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_UserManagerForm
{
public:
    QVBoxLayout *verticalLayout;
    QVBoxLayout *verticalLayout_5;
    QGridLayout *gridLayout_2;
    QSpacerItem *horizontalSpacer_10;
    QSpacerItem *horizontalSpacer_11;
    QPushButton *user;
    QLabel *label_2;
    QPushButton *logoutForm;
    QFrame *line;
    QSpacerItem *horizontalSpacer_12;
    QSpacerItem *horizontalSpacer_13;
    QComboBox *roleComboBox;
    QLabel *label_3;
    QSpacerItem *horizontalSpacer_14;
    QSpacerItem *horizontalSpacer_15;
    QTabWidget *tabWidget;
    QWidget *tab;
    QVBoxLayout *verticalLayout_4;
    QVBoxLayout *verticalLayout_3;
    QGridLayout *gridLayout;
    QSpacerItem *verticalSpacer_2;
    QSpacerItem *verticalSpacer;
    QTableView *userTableView;
    QLabel *label_7;
    QPushButton *searchUserButton;
    QLabel *label;
    QComboBox *searchComboBox;
    QPushButton *viewAllButton;
    QLineEdit *searchUserEdit;
    QComboBox *typeComboBox;
    QSpacerItem *verticalSpacer_3;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *editUserButton;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *deleteUserButton;
    QSpacerItem *horizontalSpacer_7;
    QPushButton *createUserButton;
    QSpacerItem *horizontalSpacer_8;
    QPushButton *lockButton;
    QSpacerItem *horizontalSpacer;
    QSpacerItem *horizontalSpacer_16;
    QSpacerItem *horizontalSpacer_9;
    QWidget *tab_4;
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout_10;
    QGridLayout *gridLayout_4;
    QHBoxLayout *horizontalLayout_4;
    QLineEdit *searchNotiBox;
    QPushButton *searchNotiButton;
    QTableView *notiTableView;
    QVBoxLayout *verticalLayout_9;
    QSpacerItem *verticalSpacer_10;
    QLabel *label_14;
    QComboBox *typeNotiComboBox;
    QSpacerItem *verticalSpacer_9;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_17;
    QPushButton *editNotiButton;
    QSpacerItem *horizontalSpacer_18;
    QPushButton *sendNotiButton;
    QSpacerItem *horizontalSpacer_19;
    QPushButton *deleteNotiButton;
    QSpacerItem *horizontalSpacer_20;
    QSpacerItem *horizontalSpacer_21;
    QWidget *tab_2;
    QVBoxLayout *verticalLayout_6;
    QTableView *historyTableView;
    QPushButton *pushButton;

    void setupUi(QWidget *UserManagerForm)
    {
        if (UserManagerForm->objectName().isEmpty())
            UserManagerForm->setObjectName(QStringLiteral("UserManagerForm"));
        UserManagerForm->resize(1360, 630);
        UserManagerForm->setMinimumSize(QSize(1360, 630));
        verticalLayout = new QVBoxLayout(UserManagerForm);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_10, 0, 1, 1, 1);

        horizontalSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_11, 2, 1, 1, 1);

        user = new QPushButton(UserManagerForm);
        user->setObjectName(QStringLiteral("user"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(user->sizePolicy().hasHeightForWidth());
        user->setSizePolicy(sizePolicy);
        QFont font;
        font.setPointSize(11);
        user->setFont(font);
        QIcon icon;
        icon.addFile(QStringLiteral(":/images/user.png"), QSize(), QIcon::Normal, QIcon::Off);
        user->setIcon(icon);
        user->setIconSize(QSize(24, 24));

        gridLayout_2->addWidget(user, 0, 3, 1, 1);

        label_2 = new QLabel(UserManagerForm);
        label_2->setObjectName(QStringLiteral("label_2"));
        QFont font1;
        font1.setPointSize(18);
        font1.setBold(true);
        font1.setWeight(75);
        label_2->setFont(font1);

        gridLayout_2->addWidget(label_2, 0, 2, 1, 1);

        logoutForm = new QPushButton(UserManagerForm);
        logoutForm->setObjectName(QStringLiteral("logoutForm"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(logoutForm->sizePolicy().hasHeightForWidth());
        logoutForm->setSizePolicy(sizePolicy1);
        logoutForm->setMinimumSize(QSize(180, 0));
        logoutForm->setFont(font);
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/images/signout.png"), QSize(), QIcon::Normal, QIcon::Off);
        logoutForm->setIcon(icon1);
        logoutForm->setIconSize(QSize(24, 24));

        gridLayout_2->addWidget(logoutForm, 0, 4, 1, 1);

        line = new QFrame(UserManagerForm);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line, 1, 0, 1, 5);

        horizontalSpacer_12 = new QSpacerItem(168, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_12, 2, 0, 2, 1);

        horizontalSpacer_13 = new QSpacerItem(108, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_13, 2, 2, 2, 1);

        roleComboBox = new QComboBox(UserManagerForm);
        roleComboBox->setObjectName(QStringLiteral("roleComboBox"));
        sizePolicy1.setHeightForWidth(roleComboBox->sizePolicy().hasHeightForWidth());
        roleComboBox->setSizePolicy(sizePolicy1);
        roleComboBox->setMinimumSize(QSize(180, 0));
        roleComboBox->setFont(font);
        roleComboBox->setStyleSheet(QLatin1String("#roleComboBox{\n"
"text-align : center;\n"
"}"));

        gridLayout_2->addWidget(roleComboBox, 2, 4, 2, 1);

        label_3 = new QLabel(UserManagerForm);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout_2->addWidget(label_3, 3, 3, 1, 1);

        horizontalSpacer_14 = new QSpacerItem(168, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_14, 0, 0, 1, 1);

        horizontalSpacer_15 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_15, 2, 3, 1, 1);


        verticalLayout_5->addLayout(gridLayout_2);

        tabWidget = new QTabWidget(UserManagerForm);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        QFont font2;
        font2.setPointSize(11);
        font2.setBold(true);
        font2.setWeight(75);
        tabWidget->setFont(font2);
        tabWidget->setTabShape(QTabWidget::Rounded);
        tabWidget->setIconSize(QSize(32, 32));
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        verticalLayout_4 = new QVBoxLayout(tab);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_2, 1, 1, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 4, 1, 1, 1);

        userTableView = new QTableView(tab);
        userTableView->setObjectName(QStringLiteral("userTableView"));

        gridLayout->addWidget(userTableView, 1, 0, 8, 1);

        label_7 = new QLabel(tab);
        label_7->setObjectName(QStringLiteral("label_7"));
        QFont font3;
        font3.setPointSize(9);
        font3.setBold(false);
        font3.setWeight(50);
        label_7->setFont(font3);

        gridLayout->addWidget(label_7, 5, 1, 1, 1);

        searchUserButton = new QPushButton(tab);
        searchUserButton->setObjectName(QStringLiteral("searchUserButton"));
        QFont font4;
        font4.setPointSize(11);
        font4.setBold(false);
        font4.setWeight(50);
        searchUserButton->setFont(font4);
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/images/search.png"), QSize(), QIcon::Normal, QIcon::Off);
        searchUserButton->setIcon(icon2);
        searchUserButton->setIconSize(QSize(24, 24));

        gridLayout->addWidget(searchUserButton, 0, 1, 1, 1);

        label = new QLabel(tab);
        label->setObjectName(QStringLiteral("label"));
        label->setFont(font3);

        gridLayout->addWidget(label, 2, 1, 1, 1);

        searchComboBox = new QComboBox(tab);
        searchComboBox->setObjectName(QStringLiteral("searchComboBox"));
        QFont font5;
        font5.setPointSize(10);
        font5.setBold(false);
        font5.setWeight(50);
        searchComboBox->setFont(font5);

        gridLayout->addWidget(searchComboBox, 3, 1, 1, 1);

        viewAllButton = new QPushButton(tab);
        viewAllButton->setObjectName(QStringLiteral("viewAllButton"));
        viewAllButton->setFont(font4);

        gridLayout->addWidget(viewAllButton, 8, 1, 1, 1);

        searchUserEdit = new QLineEdit(tab);
        searchUserEdit->setObjectName(QStringLiteral("searchUserEdit"));
        searchUserEdit->setFont(font);

        gridLayout->addWidget(searchUserEdit, 0, 0, 1, 1);

        typeComboBox = new QComboBox(tab);
        typeComboBox->setObjectName(QStringLiteral("typeComboBox"));
        typeComboBox->setFont(font5);

        gridLayout->addWidget(typeComboBox, 6, 1, 1, 1);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_3, 7, 1, 1, 1);


        verticalLayout_3->addLayout(gridLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_5);

        editUserButton = new QPushButton(tab);
        editUserButton->setObjectName(QStringLiteral("editUserButton"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(editUserButton->sizePolicy().hasHeightForWidth());
        editUserButton->setSizePolicy(sizePolicy2);
        editUserButton->setMinimumSize(QSize(250, 0));
        editUserButton->setFont(font);
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/images/editUser.ico"), QSize(), QIcon::Normal, QIcon::Off);
        editUserButton->setIcon(icon3);
        editUserButton->setIconSize(QSize(32, 32));

        horizontalLayout_2->addWidget(editUserButton);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_6);

        deleteUserButton = new QPushButton(tab);
        deleteUserButton->setObjectName(QStringLiteral("deleteUserButton"));
        sizePolicy2.setHeightForWidth(deleteUserButton->sizePolicy().hasHeightForWidth());
        deleteUserButton->setSizePolicy(sizePolicy2);
        deleteUserButton->setMinimumSize(QSize(180, 0));
        deleteUserButton->setFont(font);
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/images/deleteUser.ico"), QSize(), QIcon::Normal, QIcon::Off);
        deleteUserButton->setIcon(icon4);
        deleteUserButton->setIconSize(QSize(32, 32));

        horizontalLayout_2->addWidget(deleteUserButton);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_7);

        createUserButton = new QPushButton(tab);
        createUserButton->setObjectName(QStringLiteral("createUserButton"));
        sizePolicy2.setHeightForWidth(createUserButton->sizePolicy().hasHeightForWidth());
        createUserButton->setSizePolicy(sizePolicy2);
        createUserButton->setMinimumSize(QSize(180, 0));
        createUserButton->setFont(font);
        QIcon icon5;
        icon5.addFile(QStringLiteral(":/images/addUser.ico"), QSize(), QIcon::Normal, QIcon::Off);
        createUserButton->setIcon(icon5);
        createUserButton->setIconSize(QSize(32, 32));

        horizontalLayout_2->addWidget(createUserButton);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_8);

        lockButton = new QPushButton(tab);
        lockButton->setObjectName(QStringLiteral("lockButton"));
        sizePolicy2.setHeightForWidth(lockButton->sizePolicy().hasHeightForWidth());
        lockButton->setSizePolicy(sizePolicy2);
        lockButton->setMinimumSize(QSize(260, 0));
        lockButton->setFont(font);
        QIcon icon6;
        icon6.addFile(QStringLiteral(":/images/user_lock.png"), QSize(), QIcon::Normal, QIcon::Off);
        lockButton->setIcon(icon6);
        lockButton->setIconSize(QSize(32, 32));

        horizontalLayout_2->addWidget(lockButton);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        horizontalSpacer_16 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_16);

        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_9);


        verticalLayout_3->addLayout(horizontalLayout_2);


        verticalLayout_4->addLayout(verticalLayout_3);

        tabWidget->addTab(tab, icon, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QStringLiteral("tab_4"));
        verticalLayout_2 = new QVBoxLayout(tab_4);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_10 = new QVBoxLayout();
        verticalLayout_10->setObjectName(QStringLiteral("verticalLayout_10"));
        gridLayout_4 = new QGridLayout();
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        searchNotiBox = new QLineEdit(tab_4);
        searchNotiBox->setObjectName(QStringLiteral("searchNotiBox"));
        sizePolicy2.setHeightForWidth(searchNotiBox->sizePolicy().hasHeightForWidth());
        searchNotiBox->setSizePolicy(sizePolicy2);
        searchNotiBox->setMinimumSize(QSize(0, 30));
        searchNotiBox->setMaximumSize(QSize(16777215, 30));
        searchNotiBox->setFont(font);

        horizontalLayout_4->addWidget(searchNotiBox);

        searchNotiButton = new QPushButton(tab_4);
        searchNotiButton->setObjectName(QStringLiteral("searchNotiButton"));
        sizePolicy1.setHeightForWidth(searchNotiButton->sizePolicy().hasHeightForWidth());
        searchNotiButton->setSizePolicy(sizePolicy1);
        searchNotiButton->setMinimumSize(QSize(180, 30));
        searchNotiButton->setMaximumSize(QSize(180, 30));
        searchNotiButton->setFont(font);
        searchNotiButton->setIcon(icon2);
        searchNotiButton->setIconSize(QSize(24, 24));

        horizontalLayout_4->addWidget(searchNotiButton);


        gridLayout_4->addLayout(horizontalLayout_4, 0, 0, 1, 2);

        notiTableView = new QTableView(tab_4);
        notiTableView->setObjectName(QStringLiteral("notiTableView"));

        gridLayout_4->addWidget(notiTableView, 1, 0, 1, 1);

        verticalLayout_9 = new QVBoxLayout();
        verticalLayout_9->setObjectName(QStringLiteral("verticalLayout_9"));
        verticalSpacer_10 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_9->addItem(verticalSpacer_10);

        label_14 = new QLabel(tab_4);
        label_14->setObjectName(QStringLiteral("label_14"));
        QSizePolicy sizePolicy3(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(label_14->sizePolicy().hasHeightForWidth());
        label_14->setSizePolicy(sizePolicy3);
        label_14->setMinimumSize(QSize(180, 0));
        label_14->setMaximumSize(QSize(180, 16777215));
        label_14->setFont(font3);

        verticalLayout_9->addWidget(label_14);

        typeNotiComboBox = new QComboBox(tab_4);
        typeNotiComboBox->setObjectName(QStringLiteral("typeNotiComboBox"));
        sizePolicy1.setHeightForWidth(typeNotiComboBox->sizePolicy().hasHeightForWidth());
        typeNotiComboBox->setSizePolicy(sizePolicy1);
        typeNotiComboBox->setMinimumSize(QSize(180, 0));
        typeNotiComboBox->setMaximumSize(QSize(180, 16777215));
        typeNotiComboBox->setFont(font4);

        verticalLayout_9->addWidget(typeNotiComboBox);

        verticalSpacer_9 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_9->addItem(verticalSpacer_9);


        gridLayout_4->addLayout(verticalLayout_9, 1, 1, 1, 1);


        verticalLayout_10->addLayout(gridLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalSpacer_17 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_17);

        editNotiButton = new QPushButton(tab_4);
        editNotiButton->setObjectName(QStringLiteral("editNotiButton"));
        sizePolicy2.setHeightForWidth(editNotiButton->sizePolicy().hasHeightForWidth());
        editNotiButton->setSizePolicy(sizePolicy2);
        editNotiButton->setMinimumSize(QSize(300, 0));
        editNotiButton->setFont(font2);
        QIcon icon7;
        icon7.addFile(QStringLiteral(":/images/mail check.png"), QSize(), QIcon::Normal, QIcon::Off);
        editNotiButton->setIcon(icon7);
        editNotiButton->setIconSize(QSize(38, 38));

        horizontalLayout_5->addWidget(editNotiButton);

        horizontalSpacer_18 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_18);

        sendNotiButton = new QPushButton(tab_4);
        sendNotiButton->setObjectName(QStringLiteral("sendNotiButton"));
        sizePolicy2.setHeightForWidth(sendNotiButton->sizePolicy().hasHeightForWidth());
        sendNotiButton->setSizePolicy(sizePolicy2);
        sendNotiButton->setMinimumSize(QSize(200, 0));
        sendNotiButton->setFont(font2);
        QIcon icon8;
        icon8.addFile(QStringLiteral(":/images/mail send.png"), QSize(), QIcon::Normal, QIcon::Off);
        sendNotiButton->setIcon(icon8);
        sendNotiButton->setIconSize(QSize(38, 38));

        horizontalLayout_5->addWidget(sendNotiButton);

        horizontalSpacer_19 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_19);

        deleteNotiButton = new QPushButton(tab_4);
        deleteNotiButton->setObjectName(QStringLiteral("deleteNotiButton"));
        deleteNotiButton->setMinimumSize(QSize(200, 0));
        deleteNotiButton->setFont(font2);
        QIcon icon9;
        icon9.addFile(QStringLiteral(":/images/mail delete.png"), QSize(), QIcon::Normal, QIcon::Off);
        deleteNotiButton->setIcon(icon9);
        deleteNotiButton->setIconSize(QSize(38, 38));

        horizontalLayout_5->addWidget(deleteNotiButton);

        horizontalSpacer_20 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_20);

        horizontalSpacer_21 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_21);


        verticalLayout_10->addLayout(horizontalLayout_5);


        verticalLayout_2->addLayout(verticalLayout_10);

        QIcon icon10;
        icon10.addFile(QStringLiteral(":/images/mail.png"), QSize(), QIcon::Normal, QIcon::Off);
        tabWidget->addTab(tab_4, icon10, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        verticalLayout_6 = new QVBoxLayout(tab_2);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        historyTableView = new QTableView(tab_2);
        historyTableView->setObjectName(QStringLiteral("historyTableView"));

        verticalLayout_6->addWidget(historyTableView);

        pushButton = new QPushButton(tab_2);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setMinimumSize(QSize(0, 35));
        QIcon icon11;
        icon11.addFile(QStringLiteral(":/images/exit.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton->setIcon(icon11);
        pushButton->setIconSize(QSize(28, 28));

        verticalLayout_6->addWidget(pushButton);

        QIcon icon12;
        icon12.addFile(QStringLiteral(":/images/history.png"), QSize(), QIcon::Normal, QIcon::Off);
        tabWidget->addTab(tab_2, icon12, QString());

        verticalLayout_5->addWidget(tabWidget);


        verticalLayout->addLayout(verticalLayout_5);


        retranslateUi(UserManagerForm);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(UserManagerForm);
    } // setupUi

    void retranslateUi(QWidget *UserManagerForm)
    {
        UserManagerForm->setWindowTitle(QApplication::translate("UserManagerForm", "LibPro: Qu\341\272\243n l\303\275 ng\306\260\341\273\235i d\303\271ng", Q_NULLPTR));
        user->setText(QApplication::translate("UserManagerForm", "T\303\240i Kho\341\272\243n", Q_NULLPTR));
        label_2->setText(QApplication::translate("UserManagerForm", "QU\341\272\242N L\303\235 NG\306\257\341\273\234I D\303\231NG   ", Q_NULLPTR));
        logoutForm->setText(QApplication::translate("UserManagerForm", "\304\220\304\203ng Xu\341\272\245t", Q_NULLPTR));
        label_3->setText(QString());
        label_7->setText(QApplication::translate("UserManagerForm", "Danh m\341\273\245c", Q_NULLPTR));
        searchUserButton->setText(QApplication::translate("UserManagerForm", "T\303\254m ki\341\272\277m", Q_NULLPTR));
        label->setText(QApplication::translate("UserManagerForm", "T\303\254m ki\341\272\277m theo", Q_NULLPTR));
        searchComboBox->clear();
        searchComboBox->insertItems(0, QStringList()
         << QApplication::translate("UserManagerForm", "T\341\272\245t c\341\272\243", Q_NULLPTR)
         << QApplication::translate("UserManagerForm", "userName", Q_NULLPTR)
         << QApplication::translate("UserManagerForm", "H\341\273\215 T\303\252n", Q_NULLPTR)
         << QApplication::translate("UserManagerForm", "CMND", Q_NULLPTR)
         << QApplication::translate("UserManagerForm", "MSSV/GV", Q_NULLPTR)
         << QApplication::translate("UserManagerForm", "Khoa", Q_NULLPTR)
         << QApplication::translate("UserManagerForm", "Th\303\264ng tin", Q_NULLPTR)
         << QApplication::translate("UserManagerForm", "Email", Q_NULLPTR)
         << QApplication::translate("UserManagerForm", "S\341\273\221 \304\221i\341\273\207n tho\341\272\241i", Q_NULLPTR)
        );
        viewAllButton->setText(QApplication::translate("UserManagerForm", "Xem t\341\272\245t c\341\272\243", Q_NULLPTR));
        searchUserEdit->setText(QString());
        typeComboBox->clear();
        typeComboBox->insertItems(0, QStringList()
         << QApplication::translate("UserManagerForm", "T\341\272\245t c\341\272\243", Q_NULLPTR)
         << QApplication::translate("UserManagerForm", "Ch\341\273\235 t\341\272\241o", Q_NULLPTR)
         << QApplication::translate("UserManagerForm", "Ho\341\272\241t \304\221\341\273\231ng", Q_NULLPTR)
         << QApplication::translate("UserManagerForm", "B\341\273\213 kh\303\263a", Q_NULLPTR)
         << QApplication::translate("UserManagerForm", "\304\220\341\273\231c gi\341\272\243", Q_NULLPTR)
         << QApplication::translate("UserManagerForm", "Th\341\273\247 th\306\260", Q_NULLPTR)
         << QApplication::translate("UserManagerForm", "Qu\341\272\243n l\303\275 ng\306\260\341\273\235i d\303\271ng", Q_NULLPTR)
         << QApplication::translate("UserManagerForm", "SV", Q_NULLPTR)
         << QApplication::translate("UserManagerForm", "GV", Q_NULLPTR)
         << QApplication::translate("UserManagerForm", "CNVC", Q_NULLPTR)
        );
        editUserButton->setText(QApplication::translate("UserManagerForm", "Ch\341\273\211nh s\341\273\255a t\303\240i kho\341\272\243n", Q_NULLPTR));
        deleteUserButton->setText(QApplication::translate("UserManagerForm", "X\303\263a t\303\240i kho\341\272\243n", Q_NULLPTR));
        createUserButton->setText(QApplication::translate("UserManagerForm", "T\341\272\241o t\303\240i kho\341\272\243n", Q_NULLPTR));
        lockButton->setText(QApplication::translate("UserManagerForm", "Duy\341\273\207t/Kh\303\263a/M\341\273\237 kh\303\263a", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("UserManagerForm", "Ng\306\260\341\273\235i d\303\271ng", Q_NULLPTR));
        searchNotiBox->setText(QString());
        searchNotiButton->setText(QApplication::translate("UserManagerForm", "T\303\254m ki\341\272\277m", Q_NULLPTR));
        label_14->setText(QApplication::translate("UserManagerForm", "Danh m\341\273\245c", Q_NULLPTR));
        typeNotiComboBox->clear();
        typeNotiComboBox->insertItems(0, QStringList()
         << QApplication::translate("UserManagerForm", "T\341\272\245t c\341\272\243", Q_NULLPTR)
         << QApplication::translate("UserManagerForm", "Ch\306\260a \304\221\341\273\215c", Q_NULLPTR)
         << QApplication::translate("UserManagerForm", "\304\220\303\243 \304\221\341\273\215c", Q_NULLPTR)
         << QApplication::translate("UserManagerForm", "Y\303\252u c\341\272\247u h\341\273\227 tr\341\273\243", Q_NULLPTR)
         << QApplication::translate("UserManagerForm", "G\303\263p \303\275", Q_NULLPTR)
        );
        editNotiButton->setText(QApplication::translate("UserManagerForm", "Xem/ Ch\341\273\211nh s\341\273\255a th\303\264ng b\303\241o", Q_NULLPTR));
        sendNotiButton->setText(QApplication::translate("UserManagerForm", "G\341\273\255i th\303\264ng b\303\241o", Q_NULLPTR));
        deleteNotiButton->setText(QApplication::translate("UserManagerForm", "X\303\263a th\303\264ng b\303\241o", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QApplication::translate("UserManagerForm", "Th\303\264ng b\303\241o", Q_NULLPTR));
        pushButton->setText(QApplication::translate("UserManagerForm", "X\303\263a l\341\273\213ch s\341\273\255", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("UserManagerForm", "L\341\273\213ch s\341\273\255 h\341\273\207 th\341\273\221ng", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class UserManagerForm: public Ui_UserManagerForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_USERMANAGERFORM_H
