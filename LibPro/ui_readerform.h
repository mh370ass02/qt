/********************************************************************************
** Form generated from reading UI file 'readerform.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_READERFORM_H
#define UI_READERFORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ReaderForm
{
public:
    QVBoxLayout *verticalLayout_11;
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout_2;
    QPushButton *user;
    QPushButton *logoutForm;
    QFrame *line;
    QSpacerItem *horizontalSpacer_5;
    QSpacerItem *horizontalSpacer_6;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer_10;
    QSpacerItem *horizontalSpacer_8;
    QLabel *label_3;
    QComboBox *roleComboBox;
    QSpacerItem *horizontalSpacer_7;
    QSpacerItem *horizontalSpacer_11;
    QTabWidget *tabWidget;
    QWidget *tab;
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout_2;
    QWidget *hoa;
    QGridLayout *gridLayout;
    QSpacerItem *verticalSpacer_2;
    QSpacerItem *verticalSpacer_3;
    QSpacerItem *verticalSpacer_4;
    QPushButton *searchButton;
    QLabel *label_7;
    QLineEdit *searchBox;
    QLabel *label;
    QComboBox *searchComboBox;
    QLabel *label_6;
    QComboBox *sortModeComboBox;
    QPushButton *viewAllButton;
    QSpacerItem *verticalSpacer_7;
    QComboBox *sortComboBox;
    QLabel *label_9;
    QSpacerItem *verticalSpacer;
    QComboBox *typeComboBox;
    QTableView *bookTableView;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *viewMoreButton;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *borrowButton;
    QSpacerItem *horizontalSpacer_3;
    QSpacerItem *horizontalSpacer_4;
    QWidget *tab_2;
    QVBoxLayout *verticalLayout_7;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_9;
    QLabel *label_10;
    QLabel *label_5;
    QSpacerItem *horizontalSpacer_12;
    QHBoxLayout *horizontalLayout_2;
    QTableView *cartTableView;
    QVBoxLayout *verticalLayout_4;
    QSpacerItem *verticalSpacer_5;
    QPushButton *deleteCartButton;
    QSpacerItem *verticalSpacer_6;
    QPushButton *requestButton;
    QSpacerItem *verticalSpacer_8;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_13;
    QLabel *label_8;
    QLabel *label_4;
    QSpacerItem *horizontalSpacer_14;
    QHBoxLayout *horizontalLayout_3;
    QTableView *borrowTableView;
    QVBoxLayout *verticalLayout_5;
    QSpacerItem *verticalSpacer_9;
    QPushButton *deleteRequestButton;
    QSpacerItem *verticalSpacer_10;
    QWidget *tab_3;
    QVBoxLayout *verticalLayout_8;
    QTableView *historyTableView;
    QWidget *tab_4;
    QVBoxLayout *verticalLayout_10;
    QGridLayout *gridLayout_3;
    QHBoxLayout *horizontalLayout_6;
    QLineEdit *searchNotiBox;
    QPushButton *searchNotiButton;
    QTableView *notiTableView;
    QVBoxLayout *verticalLayout_9;
    QSpacerItem *verticalSpacer_12;
    QPushButton *viewNotiButton;
    QLabel *label_11;
    QPushButton *deleteNotiButton;
    QSpacerItem *verticalSpacer_11;
    QPushButton *gopYButton;
    QPushButton *hoTroButton;
    QPushButton *deNghiMuaSachButton;

    void setupUi(QWidget *ReaderForm)
    {
        if (ReaderForm->objectName().isEmpty())
            ReaderForm->setObjectName(QStringLiteral("ReaderForm"));
        ReaderForm->resize(1398, 760);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(ReaderForm->sizePolicy().hasHeightForWidth());
        ReaderForm->setSizePolicy(sizePolicy);
        ReaderForm->setMinimumSize(QSize(1360, 630));
        ReaderForm->setMaximumSize(QSize(16777215, 16777215));
        verticalLayout_11 = new QVBoxLayout(ReaderForm);
        verticalLayout_11->setSpacing(6);
        verticalLayout_11->setContentsMargins(11, 11, 11, 11);
        verticalLayout_11->setObjectName(QStringLiteral("verticalLayout_11"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        user = new QPushButton(ReaderForm);
        user->setObjectName(QStringLiteral("user"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(user->sizePolicy().hasHeightForWidth());
        user->setSizePolicy(sizePolicy1);
        QFont font;
        font.setPointSize(11);
        user->setFont(font);
        QIcon icon;
        icon.addFile(QStringLiteral(":/images/user.png"), QSize(), QIcon::Normal, QIcon::Off);
        user->setIcon(icon);
        user->setIconSize(QSize(24, 24));

        gridLayout_2->addWidget(user, 0, 3, 1, 1);

        logoutForm = new QPushButton(ReaderForm);
        logoutForm->setObjectName(QStringLiteral("logoutForm"));
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(logoutForm->sizePolicy().hasHeightForWidth());
        logoutForm->setSizePolicy(sizePolicy2);
        logoutForm->setMinimumSize(QSize(180, 0));
        logoutForm->setFont(font);
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/images/signout.png"), QSize(), QIcon::Normal, QIcon::Off);
        logoutForm->setIcon(icon1);
        logoutForm->setIconSize(QSize(24, 24));

        gridLayout_2->addWidget(logoutForm, 0, 4, 1, 1);

        line = new QFrame(ReaderForm);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line, 1, 0, 1, 5);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_5, 0, 1, 1, 1);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_6, 2, 1, 1, 1);

        label_2 = new QLabel(ReaderForm);
        label_2->setObjectName(QStringLiteral("label_2"));
        QFont font1;
        font1.setPointSize(18);
        font1.setBold(true);
        font1.setWeight(75);
        label_2->setFont(font1);

        gridLayout_2->addWidget(label_2, 0, 2, 1, 1);

        horizontalSpacer_10 = new QSpacerItem(108, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_10, 2, 2, 2, 1);

        horizontalSpacer_8 = new QSpacerItem(168, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_8, 2, 0, 2, 1);

        label_3 = new QLabel(ReaderForm);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout_2->addWidget(label_3, 3, 3, 1, 1);

        roleComboBox = new QComboBox(ReaderForm);
        roleComboBox->setObjectName(QStringLiteral("roleComboBox"));
        sizePolicy2.setHeightForWidth(roleComboBox->sizePolicy().hasHeightForWidth());
        roleComboBox->setSizePolicy(sizePolicy2);
        roleComboBox->setMinimumSize(QSize(180, 0));
        roleComboBox->setFont(font);
        roleComboBox->setStyleSheet(QLatin1String("#roleComboBox{\n"
"text-align : center;\n"
"}"));

        gridLayout_2->addWidget(roleComboBox, 2, 4, 2, 1);

        horizontalSpacer_7 = new QSpacerItem(168, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_7, 0, 0, 1, 1);

        horizontalSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_11, 2, 3, 1, 1);


        verticalLayout->addLayout(gridLayout_2);

        tabWidget = new QTabWidget(ReaderForm);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setMinimumSize(QSize(0, 0));
        QFont font2;
        font2.setPointSize(11);
        font2.setBold(true);
        font2.setWeight(75);
        tabWidget->setFont(font2);
        tabWidget->setTabShape(QTabWidget::Rounded);
        tabWidget->setIconSize(QSize(32, 32));
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        verticalLayout_3 = new QVBoxLayout(tab);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        hoa = new QWidget(tab);
        hoa->setObjectName(QStringLiteral("hoa"));
        hoa->setStyleSheet(QStringLiteral(""));
        gridLayout = new QGridLayout(hoa);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        verticalSpacer_2 = new QSpacerItem(77, 37, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_2, 8, 1, 1, 1);

        verticalSpacer_3 = new QSpacerItem(77, 37, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_3, 1, 1, 1, 1);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_4, 5, 1, 1, 1);

        searchButton = new QPushButton(hoa);
        searchButton->setObjectName(QStringLiteral("searchButton"));
        QFont font3;
        font3.setPointSize(11);
        font3.setBold(false);
        font3.setWeight(50);
        searchButton->setFont(font3);
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/images/search.png"), QSize(), QIcon::Normal, QIcon::Off);
        searchButton->setIcon(icon2);
        searchButton->setIconSize(QSize(24, 24));

        gridLayout->addWidget(searchButton, 0, 1, 1, 3);

        label_7 = new QLabel(hoa);
        label_7->setObjectName(QStringLiteral("label_7"));
        QFont font4;
        font4.setPointSize(9);
        font4.setBold(false);
        font4.setWeight(50);
        label_7->setFont(font4);

        gridLayout->addWidget(label_7, 2, 1, 1, 1);

        searchBox = new QLineEdit(hoa);
        searchBox->setObjectName(QStringLiteral("searchBox"));
        searchBox->setFont(font3);

        gridLayout->addWidget(searchBox, 0, 0, 1, 1);

        label = new QLabel(hoa);
        label->setObjectName(QStringLiteral("label"));
        label->setFont(font4);

        gridLayout->addWidget(label, 6, 1, 1, 3);

        searchComboBox = new QComboBox(hoa);
        searchComboBox->setObjectName(QStringLiteral("searchComboBox"));
        QFont font5;
        font5.setPointSize(10);
        font5.setBold(false);
        font5.setWeight(50);
        searchComboBox->setFont(font5);

        gridLayout->addWidget(searchComboBox, 7, 1, 1, 3);

        label_6 = new QLabel(hoa);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setFont(font4);

        gridLayout->addWidget(label_6, 9, 1, 1, 1);

        sortModeComboBox = new QComboBox(hoa);
        sortModeComboBox->setObjectName(QStringLiteral("sortModeComboBox"));
        sortModeComboBox->setFont(font5);

        gridLayout->addWidget(sortModeComboBox, 13, 1, 1, 3);

        viewAllButton = new QPushButton(hoa);
        viewAllButton->setObjectName(QStringLiteral("viewAllButton"));
        viewAllButton->setFont(font3);

        gridLayout->addWidget(viewAllButton, 15, 1, 1, 3);

        verticalSpacer_7 = new QSpacerItem(77, 37, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_7, 11, 1, 1, 1);

        sortComboBox = new QComboBox(hoa);
        sortComboBox->setObjectName(QStringLiteral("sortComboBox"));
        sortComboBox->setFont(font5);

        gridLayout->addWidget(sortComboBox, 10, 1, 1, 3);

        label_9 = new QLabel(hoa);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setFont(font4);

        gridLayout->addWidget(label_9, 12, 1, 1, 1);

        verticalSpacer = new QSpacerItem(121, 37, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 14, 1, 1, 2);

        typeComboBox = new QComboBox(hoa);
        typeComboBox->setObjectName(QStringLiteral("typeComboBox"));
        typeComboBox->setFont(font5);

        gridLayout->addWidget(typeComboBox, 3, 1, 1, 3);

        bookTableView = new QTableView(hoa);
        bookTableView->setObjectName(QStringLiteral("bookTableView"));
        bookTableView->setToolTipDuration(-1);
        bookTableView->setEditTriggers(QAbstractItemView::AllEditTriggers);

        gridLayout->addWidget(bookTableView, 1, 0, 15, 1);


        verticalLayout_2->addWidget(hoa);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        viewMoreButton = new QPushButton(tab);
        viewMoreButton->setObjectName(QStringLiteral("viewMoreButton"));
        QSizePolicy sizePolicy3(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(viewMoreButton->sizePolicy().hasHeightForWidth());
        viewMoreButton->setSizePolicy(sizePolicy3);
        viewMoreButton->setMaximumSize(QSize(16777215, 16777208));
        viewMoreButton->setFont(font2);
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/images/more.png"), QSize(), QIcon::Normal, QIcon::Off);
        viewMoreButton->setIcon(icon3);
        viewMoreButton->setIconSize(QSize(32, 32));

        horizontalLayout->addWidget(viewMoreButton);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        borrowButton = new QPushButton(tab);
        borrowButton->setObjectName(QStringLiteral("borrowButton"));
        sizePolicy3.setHeightForWidth(borrowButton->sizePolicy().hasHeightForWidth());
        borrowButton->setSizePolicy(sizePolicy3);
        borrowButton->setFont(font2);
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/images/returnBook.png"), QSize(), QIcon::Normal, QIcon::Off);
        borrowButton->setIcon(icon4);
        borrowButton->setIconSize(QSize(32, 32));

        horizontalLayout->addWidget(borrowButton);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_3);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_4);


        verticalLayout_2->addLayout(horizontalLayout);


        verticalLayout_3->addLayout(verticalLayout_2);

        QIcon icon5;
        icon5.addFile(QStringLiteral(":/images/book.png"), QSize(), QIcon::Normal, QIcon::Off);
        tabWidget->addTab(tab, icon5, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        verticalLayout_7 = new QVBoxLayout(tab_2);
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setContentsMargins(11, 11, 11, 11);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_9);

        label_10 = new QLabel(tab_2);
        label_10->setObjectName(QStringLiteral("label_10"));
        QSizePolicy sizePolicy4(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(label_10->sizePolicy().hasHeightForWidth());
        label_10->setSizePolicy(sizePolicy4);
        label_10->setMaximumSize(QSize(60, 60));
        QFont font6;
        font6.setPointSize(12);
        font6.setBold(true);
        font6.setWeight(75);
        label_10->setFont(font6);
        label_10->setPixmap(QPixmap(QString::fromUtf8(":/images/cart.png")));
        label_10->setScaledContents(true);

        horizontalLayout_4->addWidget(label_10);

        label_5 = new QLabel(tab_2);
        label_5->setObjectName(QStringLiteral("label_5"));
        QSizePolicy sizePolicy5(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy5.setHorizontalStretch(0);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(label_5->sizePolicy().hasHeightForWidth());
        label_5->setSizePolicy(sizePolicy5);
        label_5->setFont(font6);

        horizontalLayout_4->addWidget(label_5);

        horizontalSpacer_12 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_12);


        verticalLayout_6->addLayout(horizontalLayout_4);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        cartTableView = new QTableView(tab_2);
        cartTableView->setObjectName(QStringLiteral("cartTableView"));
        QFont font7;
        font7.setPointSize(10);
        cartTableView->setFont(font7);

        horizontalLayout_2->addWidget(cartTableView);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalSpacer_5 = new QSpacerItem(20, 28, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer_5);

        deleteCartButton = new QPushButton(tab_2);
        deleteCartButton->setObjectName(QStringLiteral("deleteCartButton"));
        sizePolicy2.setHeightForWidth(deleteCartButton->sizePolicy().hasHeightForWidth());
        deleteCartButton->setSizePolicy(sizePolicy2);
        deleteCartButton->setMinimumSize(QSize(180, 0));
        deleteCartButton->setMaximumSize(QSize(180, 16777215));
        QFont font8;
        font8.setPointSize(12);
        deleteCartButton->setFont(font8);
        QIcon icon6;
        icon6.addFile(QStringLiteral(":/images/delete cart.png"), QSize(), QIcon::Normal, QIcon::Off);
        deleteCartButton->setIcon(icon6);
        deleteCartButton->setIconSize(QSize(38, 38));

        verticalLayout_4->addWidget(deleteCartButton);

        verticalSpacer_6 = new QSpacerItem(20, 28, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer_6);

        requestButton = new QPushButton(tab_2);
        requestButton->setObjectName(QStringLiteral("requestButton"));
        sizePolicy2.setHeightForWidth(requestButton->sizePolicy().hasHeightForWidth());
        requestButton->setSizePolicy(sizePolicy2);
        requestButton->setMinimumSize(QSize(180, 0));
        requestButton->setMaximumSize(QSize(180, 16777215));
        requestButton->setFont(font8);
        QIcon icon7;
        icon7.addFile(QStringLiteral(":/images/sendReq.png"), QSize(), QIcon::Normal, QIcon::Off);
        requestButton->setIcon(icon7);
        requestButton->setIconSize(QSize(32, 32));

        verticalLayout_4->addWidget(requestButton);

        verticalSpacer_8 = new QSpacerItem(20, 28, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer_8);


        horizontalLayout_2->addLayout(verticalLayout_4);


        verticalLayout_6->addLayout(horizontalLayout_2);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalSpacer_13 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_13);

        label_8 = new QLabel(tab_2);
        label_8->setObjectName(QStringLiteral("label_8"));
        sizePolicy4.setHeightForWidth(label_8->sizePolicy().hasHeightForWidth());
        label_8->setSizePolicy(sizePolicy4);
        label_8->setMaximumSize(QSize(60, 60));
        label_8->setFont(font6);
        label_8->setPixmap(QPixmap(QString::fromUtf8(":/images/bookBorrow.png")));
        label_8->setScaledContents(true);

        horizontalLayout_5->addWidget(label_8);

        label_4 = new QLabel(tab_2);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setFont(font6);

        horizontalLayout_5->addWidget(label_4);

        horizontalSpacer_14 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_14);


        verticalLayout_6->addLayout(horizontalLayout_5);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        borrowTableView = new QTableView(tab_2);
        borrowTableView->setObjectName(QStringLiteral("borrowTableView"));
        borrowTableView->setFont(font7);

        horizontalLayout_3->addWidget(borrowTableView);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        verticalSpacer_9 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer_9);

        deleteRequestButton = new QPushButton(tab_2);
        deleteRequestButton->setObjectName(QStringLiteral("deleteRequestButton"));
        sizePolicy2.setHeightForWidth(deleteRequestButton->sizePolicy().hasHeightForWidth());
        deleteRequestButton->setSizePolicy(sizePolicy2);
        deleteRequestButton->setMinimumSize(QSize(180, 0));
        deleteRequestButton->setMaximumSize(QSize(180, 16777215));
        deleteRequestButton->setFont(font8);
        QIcon icon8;
        icon8.addFile(QStringLiteral(":/images/bin.ico"), QSize(), QIcon::Normal, QIcon::Off);
        deleteRequestButton->setIcon(icon8);
        deleteRequestButton->setIconSize(QSize(38, 38));

        verticalLayout_5->addWidget(deleteRequestButton);

        verticalSpacer_10 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer_10);


        horizontalLayout_3->addLayout(verticalLayout_5);


        verticalLayout_6->addLayout(horizontalLayout_3);


        verticalLayout_7->addLayout(verticalLayout_6);

        QIcon icon9;
        icon9.addFile(QStringLiteral(":/images/cart.png"), QSize(), QIcon::Normal, QIcon::Off);
        tabWidget->addTab(tab_2, icon9, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QStringLiteral("tab_3"));
        verticalLayout_8 = new QVBoxLayout(tab_3);
        verticalLayout_8->setSpacing(6);
        verticalLayout_8->setContentsMargins(11, 11, 11, 11);
        verticalLayout_8->setObjectName(QStringLiteral("verticalLayout_8"));
        historyTableView = new QTableView(tab_3);
        historyTableView->setObjectName(QStringLiteral("historyTableView"));

        verticalLayout_8->addWidget(historyTableView);

        QIcon icon10;
        icon10.addFile(QStringLiteral(":/images/hisBor.png"), QSize(), QIcon::Normal, QIcon::Off);
        tabWidget->addTab(tab_3, icon10, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QStringLiteral("tab_4"));
        verticalLayout_10 = new QVBoxLayout(tab_4);
        verticalLayout_10->setSpacing(6);
        verticalLayout_10->setContentsMargins(11, 11, 11, 11);
        verticalLayout_10->setObjectName(QStringLiteral("verticalLayout_10"));
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setSpacing(6);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        searchNotiBox = new QLineEdit(tab_4);
        searchNotiBox->setObjectName(QStringLiteral("searchNotiBox"));
        QSizePolicy sizePolicy6(QSizePolicy::Expanding, QSizePolicy::Maximum);
        sizePolicy6.setHorizontalStretch(0);
        sizePolicy6.setVerticalStretch(0);
        sizePolicy6.setHeightForWidth(searchNotiBox->sizePolicy().hasHeightForWidth());
        searchNotiBox->setSizePolicy(sizePolicy6);
        searchNotiBox->setMaximumSize(QSize(16777215, 45));
        searchNotiBox->setFont(font);

        horizontalLayout_6->addWidget(searchNotiBox);

        searchNotiButton = new QPushButton(tab_4);
        searchNotiButton->setObjectName(QStringLiteral("searchNotiButton"));
        QSizePolicy sizePolicy7(QSizePolicy::Fixed, QSizePolicy::Maximum);
        sizePolicy7.setHorizontalStretch(0);
        sizePolicy7.setVerticalStretch(0);
        sizePolicy7.setHeightForWidth(searchNotiButton->sizePolicy().hasHeightForWidth());
        searchNotiButton->setSizePolicy(sizePolicy7);
        searchNotiButton->setMinimumSize(QSize(220, 0));
        searchNotiButton->setMaximumSize(QSize(220, 30));
        searchNotiButton->setFont(font);
        searchNotiButton->setIcon(icon2);
        searchNotiButton->setIconSize(QSize(24, 24));

        horizontalLayout_6->addWidget(searchNotiButton);


        gridLayout_3->addLayout(horizontalLayout_6, 0, 0, 1, 2);

        notiTableView = new QTableView(tab_4);
        notiTableView->setObjectName(QStringLiteral("notiTableView"));

        gridLayout_3->addWidget(notiTableView, 1, 0, 1, 1);

        verticalLayout_9 = new QVBoxLayout();
        verticalLayout_9->setSpacing(6);
        verticalLayout_9->setObjectName(QStringLiteral("verticalLayout_9"));
        verticalSpacer_12 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_9->addItem(verticalSpacer_12);

        viewNotiButton = new QPushButton(tab_4);
        viewNotiButton->setObjectName(QStringLiteral("viewNotiButton"));
        sizePolicy7.setHeightForWidth(viewNotiButton->sizePolicy().hasHeightForWidth());
        viewNotiButton->setSizePolicy(sizePolicy7);
        viewNotiButton->setMinimumSize(QSize(220, 40));
        viewNotiButton->setMaximumSize(QSize(220, 40));
        viewNotiButton->setFont(font2);
        QIcon icon11;
        icon11.addFile(QStringLiteral(":/images/mail check.png"), QSize(), QIcon::Normal, QIcon::Off);
        viewNotiButton->setIcon(icon11);
        viewNotiButton->setIconSize(QSize(48, 48));

        verticalLayout_9->addWidget(viewNotiButton);

        label_11 = new QLabel(tab_4);
        label_11->setObjectName(QStringLiteral("label_11"));

        verticalLayout_9->addWidget(label_11);

        deleteNotiButton = new QPushButton(tab_4);
        deleteNotiButton->setObjectName(QStringLiteral("deleteNotiButton"));
        sizePolicy7.setHeightForWidth(deleteNotiButton->sizePolicy().hasHeightForWidth());
        deleteNotiButton->setSizePolicy(sizePolicy7);
        deleteNotiButton->setMinimumSize(QSize(220, 40));
        deleteNotiButton->setMaximumSize(QSize(220, 40));
        deleteNotiButton->setFont(font2);
        QIcon icon12;
        icon12.addFile(QStringLiteral(":/images/mail delete.png"), QSize(), QIcon::Normal, QIcon::Off);
        deleteNotiButton->setIcon(icon12);
        deleteNotiButton->setIconSize(QSize(48, 48));

        verticalLayout_9->addWidget(deleteNotiButton);

        verticalSpacer_11 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_9->addItem(verticalSpacer_11);

        gopYButton = new QPushButton(tab_4);
        gopYButton->setObjectName(QStringLiteral("gopYButton"));
        sizePolicy7.setHeightForWidth(gopYButton->sizePolicy().hasHeightForWidth());
        gopYButton->setSizePolicy(sizePolicy7);
        gopYButton->setMinimumSize(QSize(220, 40));
        gopYButton->setMaximumSize(QSize(220, 40));
        gopYButton->setFont(font2);
        QIcon icon13;
        icon13.addFile(QStringLiteral(":/images/feedback.png"), QSize(), QIcon::Normal, QIcon::Off);
        gopYButton->setIcon(icon13);
        gopYButton->setIconSize(QSize(36, 36));

        verticalLayout_9->addWidget(gopYButton);

        hoTroButton = new QPushButton(tab_4);
        hoTroButton->setObjectName(QStringLiteral("hoTroButton"));
        sizePolicy7.setHeightForWidth(hoTroButton->sizePolicy().hasHeightForWidth());
        hoTroButton->setSizePolicy(sizePolicy7);
        hoTroButton->setMinimumSize(QSize(220, 40));
        hoTroButton->setMaximumSize(QSize(220, 40));
        hoTroButton->setFont(font2);
        QIcon icon14;
        icon14.addFile(QStringLiteral(":/images/support.png"), QSize(), QIcon::Normal, QIcon::Off);
        hoTroButton->setIcon(icon14);
        hoTroButton->setIconSize(QSize(32, 32));

        verticalLayout_9->addWidget(hoTroButton);

        deNghiMuaSachButton = new QPushButton(tab_4);
        deNghiMuaSachButton->setObjectName(QStringLiteral("deNghiMuaSachButton"));
        sizePolicy7.setHeightForWidth(deNghiMuaSachButton->sizePolicy().hasHeightForWidth());
        deNghiMuaSachButton->setSizePolicy(sizePolicy7);
        deNghiMuaSachButton->setMinimumSize(QSize(220, 40));
        deNghiMuaSachButton->setMaximumSize(QSize(220, 40));
        deNghiMuaSachButton->setFont(font2);
        QIcon icon15;
        icon15.addFile(QStringLiteral(":/images/request.png"), QSize(), QIcon::Normal, QIcon::Off);
        deNghiMuaSachButton->setIcon(icon15);
        deNghiMuaSachButton->setIconSize(QSize(42, 42));

        verticalLayout_9->addWidget(deNghiMuaSachButton);


        gridLayout_3->addLayout(verticalLayout_9, 1, 1, 1, 1);


        verticalLayout_10->addLayout(gridLayout_3);

        QIcon icon16;
        icon16.addFile(QStringLiteral(":/images/mail.png"), QSize(), QIcon::Normal, QIcon::Off);
        tabWidget->addTab(tab_4, icon16, QString());

        verticalLayout->addWidget(tabWidget);


        verticalLayout_11->addLayout(verticalLayout);


        retranslateUi(ReaderForm);

        tabWidget->setCurrentIndex(3);


        QMetaObject::connectSlotsByName(ReaderForm);
    } // setupUi

    void retranslateUi(QWidget *ReaderForm)
    {
        ReaderForm->setWindowTitle(QApplication::translate("ReaderForm", "LibPro: \304\220\341\273\231c gi\341\272\243", Q_NULLPTR));
        user->setText(QApplication::translate("ReaderForm", "T\303\240i Kho\341\272\243n", Q_NULLPTR));
        logoutForm->setText(QApplication::translate("ReaderForm", "\304\220\304\203ng Xu\341\272\245t", Q_NULLPTR));
        label_2->setText(QApplication::translate("ReaderForm", "\304\220\341\273\230C GI\341\272\242", Q_NULLPTR));
        label_3->setText(QString());
        searchButton->setText(QApplication::translate("ReaderForm", "T\303\254m ki\341\272\277m", Q_NULLPTR));
        label_7->setText(QApplication::translate("ReaderForm", "Danh m\341\273\245c", Q_NULLPTR));
        searchBox->setText(QString());
        label->setText(QApplication::translate("ReaderForm", "T\303\254m ki\341\272\277m theo", Q_NULLPTR));
        searchComboBox->clear();
        searchComboBox->insertItems(0, QStringList()
         << QApplication::translate("ReaderForm", "T\341\272\245t c\341\272\243", Q_NULLPTR)
         << QApplication::translate("ReaderForm", "M\303\243 s\303\241ch", Q_NULLPTR)
         << QApplication::translate("ReaderForm", "T\303\252n s\303\241ch", Q_NULLPTR)
         << QApplication::translate("ReaderForm", "Ch\341\273\247 \304\221\341\273\201", Q_NULLPTR)
         << QApplication::translate("ReaderForm", "T\303\252n t\303\241c gi\341\272\243", Q_NULLPTR)
         << QApplication::translate("ReaderForm", "Nh\303\240 xu\341\272\245t b\341\272\243n", Q_NULLPTR)
         << QApplication::translate("ReaderForm", "N\304\203m xu\341\272\245t b\341\272\243n", Q_NULLPTR)
         << QApplication::translate("ReaderForm", "S\341\273\221 s\303\241ch \304\221\303\243 m\306\260\341\273\243n", Q_NULLPTR)
         << QApplication::translate("ReaderForm", "T\303\263m t\341\272\257t n\341\273\231i dung", Q_NULLPTR)
        );
        label_6->setText(QApplication::translate("ReaderForm", "S\341\272\257p x\341\272\277p theo:", Q_NULLPTR));
        sortModeComboBox->clear();
        sortModeComboBox->insertItems(0, QStringList()
         << QApplication::translate("ReaderForm", "T\304\203ng d\341\272\247n", Q_NULLPTR)
         << QApplication::translate("ReaderForm", "Gi\341\272\243m d\341\272\247n", Q_NULLPTR)
        );
        viewAllButton->setText(QApplication::translate("ReaderForm", "Xem t\341\272\245t c\341\272\243", Q_NULLPTR));
        sortComboBox->clear();
        sortComboBox->insertItems(0, QStringList()
         << QApplication::translate("ReaderForm", "M\341\273\233i v\341\273\201", Q_NULLPTR)
         << QApplication::translate("ReaderForm", "M\303\243 s\303\241ch", Q_NULLPTR)
         << QApplication::translate("ReaderForm", "T\303\252n s\303\241ch", Q_NULLPTR)
         << QApplication::translate("ReaderForm", "Lo\341\272\241i s\303\241ch", Q_NULLPTR)
         << QApplication::translate("ReaderForm", "T\303\252n t\303\241c gi\341\272\243", Q_NULLPTR)
         << QApplication::translate("ReaderForm", "Nh\303\240 xu\341\272\245t b\341\272\243n", Q_NULLPTR)
         << QApplication::translate("ReaderForm", "N\304\203m xu\341\272\245t b\341\272\243n", Q_NULLPTR)
         << QApplication::translate("ReaderForm", "S\341\273\221 s\303\241ch \304\221\303\243 m\306\260\341\273\243n", Q_NULLPTR)
        );
        label_9->setText(QApplication::translate("ReaderForm", "Th\341\273\251 t\341\273\261:", Q_NULLPTR));
        typeComboBox->clear();
        typeComboBox->insertItems(0, QStringList()
         << QApplication::translate("ReaderForm", "T\341\272\245t c\341\272\243", Q_NULLPTR)
         << QApplication::translate("ReaderForm", "S\303\241ch", Q_NULLPTR)
         << QApplication::translate("ReaderForm", "T\341\272\241p ch\303\255", Q_NULLPTR)
         << QApplication::translate("ReaderForm", "Lu\341\272\255n \303\241n ti\341\272\277n s\304\251", Q_NULLPTR)
         << QApplication::translate("ReaderForm", "Lu\341\272\255n v\304\203n th\341\272\241c s\304\251", Q_NULLPTR)
         << QApplication::translate("ReaderForm", "Ti\303\252u chu\341\272\251n k\341\273\271 thu\341\272\255t", Q_NULLPTR)
         << QApplication::translate("ReaderForm", "B\303\241o c\303\241o khoa h\341\273\215c", Q_NULLPTR)
        );
        viewMoreButton->setText(QApplication::translate("ReaderForm", "Xem th\303\252m", Q_NULLPTR));
        borrowButton->setText(QApplication::translate("ReaderForm", "Th\303\252m v\303\240o gi\341\273\217", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("ReaderForm", "S\303\241ch", Q_NULLPTR));
        label_10->setText(QString());
        label_5->setText(QApplication::translate("ReaderForm", "Gi\341\273\217 s\303\241ch", Q_NULLPTR));
        deleteCartButton->setText(QApplication::translate("ReaderForm", "X\303\263a kh\341\273\217i gi\341\273\217", Q_NULLPTR));
        requestButton->setText(QApplication::translate("ReaderForm", "G\341\273\255i y\303\252u c\341\272\247u", Q_NULLPTR));
        label_8->setText(QString());
        label_4->setText(QApplication::translate("ReaderForm", "M\306\260\341\273\243n s\303\241ch", Q_NULLPTR));
        deleteRequestButton->setText(QApplication::translate("ReaderForm", "X\303\263a y\303\252u c\341\272\247u", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("ReaderForm", "Gi\341\273\217 s\303\241ch v\303\240 M\306\260\341\273\243n s\303\241ch", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QApplication::translate("ReaderForm", "L\341\273\213ch s\341\273\255 m\306\260\341\273\243n s\303\241ch", Q_NULLPTR));
        searchNotiBox->setText(QString());
        searchNotiButton->setText(QApplication::translate("ReaderForm", "T\303\254m ki\341\272\277m", Q_NULLPTR));
        viewNotiButton->setText(QApplication::translate("ReaderForm", "Xem th\303\264ng b\303\241o", Q_NULLPTR));
        label_11->setText(QString());
        deleteNotiButton->setText(QApplication::translate("ReaderForm", "X\303\263a th\303\264ng b\303\241o", Q_NULLPTR));
        gopYButton->setText(QApplication::translate("ReaderForm", "G\303\263p \303\275", Q_NULLPTR));
        hoTroButton->setText(QApplication::translate("ReaderForm", "Y\303\252u c\341\272\247u h\341\273\227 tr\341\273\243", Q_NULLPTR));
        deNghiMuaSachButton->setText(QApplication::translate("ReaderForm", "\304\220\341\273\201 ngh\341\273\213 mua t\303\240i li\341\273\207u", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QApplication::translate("ReaderForm", "Th\303\264ng b\303\241o", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class ReaderForm: public Ui_ReaderForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_READERFORM_H
