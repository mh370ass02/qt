#include "adduserform.h"
#include "ui_adduserform.h"
#include <QMessageBox>
#include <QDebug>

AddUserForm::AddUserForm(QWidget *parent, int mode) :
    QWidget(parent),
    ui(new Ui::AddUserForm)
{
    ui->setupUi(this);
    if(mode == 1)
    {
        ui->readerCheck->hide();
        ui->librarianCheck->hide();
        ui->userManaCheck->hide();
        ui->createButton->setText("Đăng ký");
        ui->createLabel->setText("Đăng ký người dùng mới");
    }
}

AddUserForm::~AddUserForm()
{
    delete ui;
}

void AddUserForm::closeEvent (QCloseEvent *event)
{
    /*extern AddUserForm *addUserForm;
    QMessageBox::StandardButton resBtn = QMessageBox::critical(this, "exit but",
                                                                tr("Are you sure?\n"),
                                                                QMessageBox::Cancel | QMessageBox::No | QMessageBox::Yes,
                                                                QMessageBox::Yes);
    if (resBtn != QMessageBox::Yes) {
        event->ignore();
    } else {
        event->accept();
    }*/
}

void AddUserForm::on_createButton_clicked()
{
    QString userName = ui->userName->text(),
            passWord = ui->passWord->text(),
            hoTen = ui->hoTen->text(),
            ngaySinh = ui->ngaySinh->text(),
            CMND = ui->CMND->text(),
            MSSV = ui->MSSV->text(),
            email = ui->email->text(),
            khoa = ui->khoa->text(),
            diaChi = ui->diaChi->text(),
            thongTin = ui->thongTin->text();

    if (userName.isEmpty() || passWord.isEmpty() || hoTen.isEmpty() || ngaySinh.isEmpty() ||
        CMND.isEmpty() || MSSV.isEmpty() || email.isEmpty() || khoa.isEmpty() || diaChi.isEmpty())
    {
        QMessageBox::critical(this,"Lỗi đăng ký", "Không thể tạo tài khoản\n"
                                                  "Bạn điền thiếu thông tin");
        return;
    }
    query.exec("SELECT COUNT(*) FROM user WHERE user.CMND = '"+CMND+"'");
    query.first();
    if(query.value(0).toInt() > 0)
    {
        QMessageBox::critical(this,"Lỗi đăng ký", "Không thể tạo tài khoản\n"
                                                  "Người dùng này đã tồn tại");
        return;
    }
    query.exec("SELECT COUNT(*) FROM user WHERE user.MSSV = '"+MSSV+"'");
    query.first();
    if(query.value(0).toInt() > 0)
    {
        QMessageBox::critical(this,"Lỗi đăng ký", "Không thể tạo tài khoản\n"
                                                  "Sinh viên này đã tồn tại");
        return;
    }
    query.exec("SELECT COUNT(*) FROM user WHERE user.userName = '"+userName+"'");
    query.first();
    if(query.value(0).toInt() > 0)
    {
        QMessageBox::critical(this,"Lỗi đăng ký", "Không thể tạo tài khoản\n"
                                                  "Tên tài khoản này đã tồn tại");
        return;
    }

    if (!query.exec("INSERT INTO user VALUES (1,-1,'"+userName+"','"+passWord+"','"+hoTen+"','"+
               ngaySinh+"','"+CMND+"','"+MSSV+"','"+email+"','"+khoa+"','"+diaChi+"','"+thongTin+"')"))
    {
        QMessageBox::critical(this,"Lỗi đăng ký", "Không thể tạo tài khoản\n");
    }
    //query.exec("COMMIT");
    QMessageBox::information(this, "Thông báo", "Tài khoản " + userName +
                             " đã được đăng ký thành công và đang chờ xét duyệt");
    hide();

}

void AddUserForm::start()
{
    ui->userName->clear();
    ui->passWord->clear();
    ui->hoTen->clear();
    ui->ngaySinh->clear();
    ui->CMND->clear();
    ui->MSSV->clear();
    ui->email->clear();
    ui->khoa->clear();
    ui->diaChi->clear();
    ui->thongTin->clear();
    ui->readerCheck->setChecked(true);
    ui->librarianCheck->setChecked(false);
    ui->userManaCheck->setChecked(false);
    show();
}
